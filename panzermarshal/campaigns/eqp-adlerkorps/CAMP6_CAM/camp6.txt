Russian Counter-Offensive Campaign (1942-1946)

Command the Russian Counter Offensive as Marshal of the Soviet Union Georgy Konstantinovich Zhukov starting with the battle of Stalingrad and then leading your
army in Operation Uranus, Saturn and liberate the key cities of Rostov, Sevastopol, Odessa, 
Kharkov, Kanev, Kiev. Stop German counter attacks and continue to control the South-West front to take Berlin and be the architect of the Fall of the Reichstag. 

As the historical context ends with the Fall of the Reichstag you have the posibility to continue the war to conquer the entire Europe, United-Kingdom and finally the United States.

Can you succeed ?

Original Author: Tim Ruger
