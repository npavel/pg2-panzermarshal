[MAP:242] [KISHINE2.map        ]
--------------------------------
(05,00) #Jijia River
(07,00) #Prut River
(09,00) #Ungheni
(05,01) #Jijia River
(06,01) #Prut River
(07,01) #Prut River
(08,01) #Ungheni
(09,01) #Ungheni
(36,01) #Bravicea
(37,01) #Bravicea
(03,02) #Jijia River
(04,02) #Jijia River
(07,02) #Prut River
(04,03) #Jijia River
(06,03) #Prut River
(07,03) #Prut River
(09,03) #Valea Mare
(04,04) #Jijia River
(05,04) #Jijia River
(08,04) #Prut River
(09,04) #Valea Mare
(10,04) #Valea Mare
(32,04) #Raciula
(33,04) #Raciula
(05,05) #Jijia River
(07,05) #Prut River
(08,05) #Prut River
(17,05) #Milesti
(20,05) #Temeleuti
(25,05) #Sipoteni
(00,06) #Jijia River
(06,06) #Jijia River
(07,06) #Prut River
(23,06) #Singera
(25,06) #Sipoteni
(26,06) #Sipoteni
(29,06) #Calarasi
(06,07) #Jijia River
(07,07) #Jijia River
(08,07) #Prut River
(29,07) #Calarasi
(07,08) #Jijia River
(08,08) #Prut River
(09,08) #Prut River
(03,09) #Yassy
(07,09) #Jijia River
(09,09) #Prut River
(41,09) #Ichel River
(03,10) #Yassy
(07,10) #Jijia River
(10,10) #Prut River
(41,10) #Ichel River
(43,10) #Ichel River
(06,11) #Jijia River
(09,11) #Prut River
(10,11) #Prut River
(26,11) #Bucovat
(27,11) #Bucovat
(32,11) #Bucovat
(33,11) #Bucovat
(42,11) #Ichel River
(06,12) #Jijia River
(07,12) #Jijia River
(10,12) #Prut River
(27,12) #Bucovat
(32,12) #Bucovat
(07,13) #Jijia River
(09,13) #Prut River
(10,13) #Prut River
(17,13) #Nisiporeni
(27,13) #Vorniceni
(28,13) #Vorniceni
(29,13) #Vorniceni
(39,13) #Recea
(43,13) #Ichel River
(08,14) #Jijia River
(09,14) #Prut River
(18,14) #Nisiporeni
(28,14) #Vorniceni
(39,14) #Recea
(40,14) #Recea
(42,14) #Ichel River
(43,14) #Ichel River
(08,15) #Jijia River
(10,15) #Prut River
(11,15) #Barboeni
(12,15) #Barboeni
(19,15) #Jurceni
(42,15) #Radeni
(08,16) #Jijia River
(09,16) #Jijia River
(10,16) #Prut River
(42,16) #Radeni
(43,16) #Ichel River
(08,17) #Jijia River
(10,17) #Prut River
(35,17) #Straseni
(42,17) #Ichel River
(43,17) #Ichel River
(08,18) #Jijia River
(09,18) #Jijia River
(10,18) #Prut River
(36,18) #Straseni
(09,19) #Jijia River
(10,19) #Prut River
(08,20) #Gorban
(09,20) #Gorban
(10,20) #Prut River
(25,20) #Cogilnic River
(06,21) #Radeni
(09,21) #Prut River
(10,21) #Cotu Mori
(11,21) #Cotu Mori
(25,21) #Manoilesti
(39,21) #Ciopleni
(40,21) #Ciopleni
(09,22) #Prut River
(24,22) #Cogilnic River
(25,22) #Manoilesti
(40,22) #Ciopleni
(09,23) #Prut River
(15,23) #Onesti
(25,23) #Cogilnic River
(26,23) #Cogilnic River
(38,23) #Cojusna
(08,24) #Prut River
(16,24) #Onesti
(25,24) #Cogilnic River
(38,24) #Cojusna
(02,25) #Prut River
(08,25) #Prut River
(09,25) #Prut River
(26,25) #Cogilnic River
(27,25) #Cogilnic River
(09,26) #Prut River
(27,26) #Cogilnic River
(40,26) #Kishinev
(09,27) #Prut River
(12,27) #Leuseni
(13,27) #Leuseni
(25,27) #Loganesti
(26,27) #Cogilnic River
(40,27) #Kishinev
(41,27) #Kishinev
(09,28) #Prut River
(26,28) #Cogilnic River
(27,28) #Nimoreni
(33,28) #Nimoreni
(40,28) #Kishinev
(41,28) #Kishinev
(07,29) #Prut River
(08,29) #Prut River
(26,29) #Cogilnic River
(33,29) #Nimoreni
(40,29) #Kishinev
(43,29) #Bucovat
(06,30) #Prut River
(11,30) #Cioara
(26,30) #Cogilnic River
(27,30) #Cogilnic River
(06,31) #Prut River
(07,31) #Prut River
(10,31) #Cioara
(15,31) #Carpineni
(27,31) #Cogilnic River
(37,31) #Ioloveni
(08,32) #Prut River
(15,32) #Carpineni
(16,32) #Carpineni
(25,32) #Hincesti
(26,32) #Cogilnic River
(27,32) #Cogilnic River
(36,32) #Ioloveni
(37,32) #Ioloveni
(08,33) #Prut River
(13,33) #Mingir
(26,33) #Hincesti
(28,33) #Cogilnic River
(07,34) #Prut River
(08,34) #Prut River
(13,34) #Mingir
(14,34) #Mingir
(24,34) #Mereseni
(27,34) #Cogilnic River
(28,34) #Cogilnic River
(33,34) #Costesti
(34,34) #Costesti
(07,35) #Prut River
(24,35) #Mereseni
(28,35) #Cogilnic River
(29,35) #Cogilnic River
(34,35) #Costesti
(40,35) #Singera
(41,35) #Singera
(06,36) #Prut River
(07,36) #Prut River
(28,36) #Cogilnic River
(08,37) #Prut River
(27,37) #Cogilnic River
(28,37) #Cogilnic River
(08,38) #Prut River
(27,38) #Cogilnic River
(28,38) #Cogilnic River
