1st SS-Panzer Division LSSAH, 
SS-Brigadefuhrer Theodor Wisch.

The Battle of Caen. 
July 17th, 1944. 
Operation Goodwood. 

On 11th July, Leibstandarte took over the Caen sector from the seriously depleted Hitlerjugend.  Here, over the coming week it played its most crucial role in the Normandy battles.  Beginning on 17th July, three British armoured divisions attempted to break through the gap between Caen and the eastern heights, across a small bridgehead over the Orne River and through the four lines of man-made and natural defences, then across the hills of Bourgebus and to the open ground beyond.

(If playing with rules do not buy any new units, except to replace any core losses.  You should deploy 6 'SS Panzergrenadiere/Infanterie', 2 Tanks, 4 Artillery, 1 Recon, 1 Air Defence/Flak, 1 'Sturmpioniere' and 2 anti-tank units.)

(HINT:  This is supposed to be difficult.  Run for the hills.  Literally.)
