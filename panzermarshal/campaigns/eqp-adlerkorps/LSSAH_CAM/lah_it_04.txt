Closing on Dunkirk
POLDER
St. Pierre Brouck
Dunkerke
Wormhoudt
Rexpoede
Poperinghe
Robecq
Merville
Arneke
Dunkerke
St. Omer
Bethune
La Bassee
Lestrem
Estaires
Noordschote
Furnes
Bray Dunes
Soex
Ledrinhem
St. Venant
Mardick
Gravelines
12 Schzt.Bde./4 Pz.Div./[XVI]
10 Pz.R./8 Pz.Div./[XXXXI]
Thule/SS[T]/[XVI]
2/SST
3/SST
SST
3/3
12/3
72/3
6/5
15/5
78/5
224/68
225/68
341/68
89/68
DG
DG
4th Bde/2nd Div./[BEF]
5th Bde/2nd Div./[BEF]
6th Bde/2nd Div./[BEF]
2nd Rgt./2nd Div./[BEF]
69th Bde/23rd Div./[BEF]
70th Bde./23rd Div./[BEF]
23rd Rgt./23rd Div./[BEF]
131st Bde./44 Div./[BEF]
132/44 [BEF]
133/44 [BEF]
44 [BEF]
137th Bde./46th Div./[BEF]
138/46 [BEF]
139/46 [BEF]
46 [BEF]
3rd Bde./1st Arm.Div/[BEF]
150th Bde./50th Div./[BEF]
151/50 [BEF]
50th/[BEF]
143rd Bde./48th Div./[BEF]
144th Bde./48th Div./[BEF]
145th Bde./48th Div./[BEF]
48th/[BEF]
1ERY/1R [BEF]
1FFY/1R [BEF]
3/2C
3D/3C
73/2C
POLDER
Bourbourgville
POLDER
POLDER
POLDER
Watten
St. Momelin
Blaringhem
27/12
48R/12 ID/[II]
89R/12 ID/[II]
12
Aire
Hinges
JG20/[L1]
L1
FC
E [BEF]
POLDER
FA
FA
289/68
RAF
[RAF]
68
Aq Canal
Lys Canal
Lys River
Lys Canal
Armentieres
Yser River
Aq River
Bergues
Cassel
Caestre
DG3
DG4
DG
3 Pz.Bde./3 Pz.Div./[XVI]
3 Schtz.Bde./3 Pz.Div./[XVI]
3 Pz.Bde./3 Pz.Div./[XVI]
75 AR/3 Pz.Div./[XVI]
103 AR/4 Pz.Div./[XVI]
5 Pz.Bde./4 Pz.Div./[XVI]
3 Auf./3 Pz.Div./[XVI]
39 Pion./3 Pz.Div./[XVI]
5 Pz.Bde./4 Pz.Div./[XVI]
79 Pion./4 Pz.Div./[XVI]
79 Pion.(B)/4 Pz.Div./[XVI]
1 Pz.Bde./1 Pz.Div./[XIX]
7 Auf./4 Pz.Div./[XVI]
1 Pz.Bde./1 Pz.Div./[XIX]
1 Schzt/1 Pz.Div./[XIX]
37 Pion(B)/1 Pz.Div./[XIX]
73 AR/1 Pz.Div./[XIX]
1 Auf./1 Pz.Div./[XIX]
8 Schzt.Bde./8 Pz.Div./[XXXXI]
80 AR/8 Pz.Div./[XXXXI]
59 Pion./8 Pz.Div./[XXXXI]
43 Pzjgr./8 Pz.Div./[XXXXI]
25 Pz.R./25 Pz.Div./[IV]
78 AR/25 Pz.Div./[IV]
7 Schzt.Bde./25 Pz.Div./[IV]
5 Auf./2 Pz.Div./[XIX]
2 Pz.Bde./2 Pz.Div./[XIX]
38 Pion./2 Pz.Div./[XIX]
38 Pion.(B)/2 Pz.Div./[XIX]
2 Mot./2 Pz.Div./[XIX]
1 AR/2 Pz.Div./[XIX]
2 Schzt.Bde./2 Pz.Div./[XIX]
11 Pz.R./6 Pz.Div./[XXXXI]
57 Pion.(B)/6 Pz.Div./[XXXXI]
76 AR/6 Pz.Div./[XXXXI]
41 Pzjgr./6 Pz.Div./[XXXXI]
4 Schzt.Bde./6 Pz.Div./[XXXXI]
SS[LAH]/[XXXXI]
Eicke/SS[T]/[XVI]
2R/SS[T]/[XVI]
StG1/[L1]
StG2/[L1]
48R/12 ID/[II]
12 AR/12 ID/[II]
132nd Bde./44 Div./[BEF]
133rdBde./44 Div./[BEF]
133rd Rgt./44 Div./[BEF]
138th Bde./46th Div./[BEF]
139th Bde./46th Div./[BEF]
139th Rgt./46th Div./[BEF]
[RAF]
[RAF]
GC III/2/FA 101
GRI/14/FA 101
151th Bde./50th Div./[BEF]
Dunkirk
Ypern
48th/[BEF]
JG20/[L1]
StG2/[L1]
StG1/[L1]
