
Meine Ehre heisst Treue - 1st SS 'Leibstandarte Adolf Hitler' Division (1939-1945) campaign.

Original authors and credits:
Immortal technique
Stephen De Jong (Warta Crossing and Bzura Breakout Scenario's)
Dirk aka. 1 Terminator (Playtester)
armygroupdf (playtester/saviour)
Adler (great e-file and a huge help)
Leon (Soviet OOB's)
Jan Hedstrom (Playtester/help with prestige)

Porting to OpenPanzer:
Alexander Sayenko (alexander.sayenko@gmail.com)

Additional OpenPanzer testing:


CHANGELOG
- original campaign SCN files are taken
- SCN files are polished, file names are aligned and file references are corrected
- in some SCN files, references to MAP files are updated
- "Advance on Dunkirk" is replaced with a completely new scenario reflecting better historical events 
- campaign description text is updated
- Epsom and Charnwood scenario end scenario description text is updated
- deployment hexes are added to Caen and Balaton scenarios
- a new scenario "Rotterdam" is added
- minor German unit fixes in the Skopje scenario
- German and Greek unit fixes in the Vevi scenario
- Default German units are added to the "Metsovon pass" scenario (Mar 13, 2019)
- "Dubno" scenario is heavily revised
- "Uman encirclement" scenario is heavily revised
- "Kherson" scenario is completely re-designed with a new map (Sep 25, 2020)
- "Melitopol counter-attack" scenario is completely re-designed with a new map (Nov 28, 2020)

TODO:
- Warsaw (new scenario, can be borrowed from Fall Weiss)
- Kastoria lake (new Greek scenario, no map)
- Peloppones (new Greek scenario, map exists)
- Uman (revise the existing scenario, new map exists)
- Zhitomyr / Stalin defense line 1941 (new Soviet scenario, no map)
- Kakhovka (new scenario, map exists)
- Taganrog (new scenario, no map, a new map might be designed soon)
- Mius (new defensive scenario, no map, Taganrog winter map could be used for it)
- Kharkov defense (revise existing scenario)
- 3rd battle for Kharkov (new scenario, can be borrowed from DasReich)
- Po river valley (new scenario, map exists)
- Kamenets-Podolski (new scenario, map exists)
- Operation Luttich (new scenario, can be borrowed from DasReich)
- Falaise Gap (new scenario, can be borrowed from DasReich)
- Balaton offensive (new scenario, map exists)
- Vienna (new scenario)
