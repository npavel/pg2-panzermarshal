Infanterie Regiment (mot) LSSAH, 
SS-Obergruppenfuhrer Joseff Dietrich.

Fall Weiss. 
4th September, 1939. 
Warta Crossing.

The invasion of Poland is in its fourth day. While German panzer divisions are spearheading toward Warsaw, the German 8th Army has reached the river Warta. 
The LSSAH was given the order to cross the river in the south and to advance on Burzenin.
However, the Poles have set up a strong defensive line and await the German assault.

(If playing with rules do not buy any new units, except to replace any core losses)

