Infanterie Regiment (mot) LSSAH, 
SS-Obergruppenfuhrer Joseff Dietrich.

Fall Weiss. 
15th September, 1939. 
Bzura Pocket.

After the Polish attacks, the Leibstandarte was withdrawn to the west, where it was attached to the forces attempting to encircle the Polish forces on the Bzura.  Under XVI Corps, the Leibstandarte played a major part in this operation, which was the largest battle of the Polish campaign.  Some of the heaviest fighting took place in the streets of Sochaczev, where strong resistance was encoutered.

The Battle of Bzura was distinguished by the great bravery of the trapped Polish forces, who by now facing annihilation, put up a spirited defence.  By the 19th however, the jaws closed around them.

(If playing according to the rules, do not buy additional units)


