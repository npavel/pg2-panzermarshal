Gran Bridgehead
Canal
Ersekujvar
Zsitva Canal
Udvard
Jaszfalu
Kural
Garam
Kisolved
Nyitra
Ipolypaszto
Ipoly
258 m (HILL)
262 m (HILL)
Nagyolved
Kety
Csatta
Paris Canal
Csuz
275 m (HILL)
219 m (HILL)
Oromhegy 182 m (ROUGH)
Fur
Kisgyarmat
230 m (HILL)
Naszvad
Bajcs
238 m (HILL)
272 m (HILL)
Bart
Beny
Kiskeszi
Airfield Imely
Nemetszogyeny
Imely
167 m (ROUGH)
Perbete
Kurt
Magyarszogyeny
190 m (ROUGH)
Kemend
Pald
O-Gyalla
Bagota
188 m (ROUGH)
390 m (HILL)
Szalka
Martos
178 m (ROUGH)
Uj-Gyalla
185 m (ROUGH)
Kovecses-hegy 285 m (HILL)
210 m (HILL)
Hegy-hat 271 m  (HILL)
196 m (HILL)
Kis-Ujfalu
Bajta
Leled
Szt. Peter
204 m (HILL)
Batorkeszi
Somlyo-hegy 226 m (HILL)
Kobolkut
217 m (HILL)
Libad
Belai-hegy 250 m (HILL)
Kohidgyarmat
Kicsind
267 m (HILL)
Bela
Garamkovesd
Madar
239 m (HILL)
Duna
Vag
Bucs
Nana
Parkany
Esztergom
Zsitva
Heteny
Marcellhaza
Muzsla
Anstalt
Kurtakeszi
Ebed
Komarom
Karva
Izsa
Mocs
Sutto
Nyergesujfalu
Uj-Szony
Factory
Labatlan
Tat
309 m (HILL)
O-Szony
Dunaalmas
Neszmely
278 m (HILL)
Bajot
Oreg-ko 374 m (HILL)
Mogyoros
294 m (HILL)
375 m (HILL)
297 m (HILL)
311 m (HILL)
Tokod
Nagy-Gete 455 m (HILL)
Korteles 335 m (HILL)
Dorog
I+II/42/46th Infanterie Div.
306/211th Volksgrenadier Div.
I+II/72/46th Infanterie Div.
317/211th Volksgrenadier Div.
I+II/97/46th Infanterie Div.
365/211th Volksgrenadier Div.
211/211th Volksgrenadier Div.
116/46th Infanterie Div.
6th Airborne Div.
I/RHD/44th Reichs-Grenadier
II/RHD/44th Reichs-Grenadier
III/RHD/44th Reichs-Grenadier
96/44th Reichs-Grenadier
80/44th Reichs-Grenadier
72nd Guards Division
409th Rifle Division
375th Rifle Division
27th Guards Tank Brigade
93rd Guards Rifle Division
Hupe Regimental Group
I+II/284/KG Hupe
II/287/KG Hupe
KG Hupe
I/26-SS/Hitlerjugend
II/26-SS/Hitlerjugend
III/26-SS/Hitlerjugend
60th Engineer Brigade
I/25-SS/Hitlerjugend
II/25-SS/Hitlerjugend
III/25-SS/Hitlerjugend
81st Guards Rifle Division
[Blown Bridge]
Do not go this way!
