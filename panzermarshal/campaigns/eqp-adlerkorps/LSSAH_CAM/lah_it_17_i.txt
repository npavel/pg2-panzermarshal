1st SS-Panzer Division LSSAH, 
SS-Brigadefuhrer Otto Kumm.

Lake Balaton Offensive. 
March 6th, 1945. 
Operation Fruhlingserwachen. 

The aims of Operation Spring Awakening were as wildly ambitious as those laid out for the Ardennes offensive three months previously.  They were: to destroy the Soviet forces in the region bounded by Lake Balaton and the Danube and Drava rivers, secure the Hungarian oil deposits, and establish bridgeheads over the Danube with a view to further offensive operations.

Whilst the Germans prepared Fruhlingerwachen, the Soviets had their own plans - an attack aimed toward Vienna, to begin on 15th March.  Following the Gran bridgehead fighting however, the Soviets became aware of German intentions and were able to adapt their positions to meet it.  The Third Ukranian Front commander - where the Leibstandarte attack was positioned - reasoned that the Germans could be sufficiently weakened by the depth of the Soviet defences, for them to be able to launch their own attack.

(If playing with rules do not buy any new units, except to replace any core losses.  You should deploy 6 'SS Panzergrenadiere/Infanterie', 2 Tanks, 4 Artillery, 1 Recon, 1 Air Defence/Flak, 1 'Sturmpioniere' and 2 anti-tank units.)
