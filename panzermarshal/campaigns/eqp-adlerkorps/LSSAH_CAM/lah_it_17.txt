The Balaton Offensive
Gem-hegy (318 m) [HILL]
Csakvar
Vali-viz
Soskut
Diosd
Duna
Gyuro
Csakvar Airfield
Vertesacsa
Val
Tarnok
Erd-Ujfalu
Erd-Ofalu
Haraszti-erdo [FOREST]
Halasztelek
Tordas
Szazhalombatta
Kajaszoszt.peter
Szigethalom
Zamoly
Lovasbereny
Vereb
Martonvasar
Dunafured
Tokol
Szigetszt.miklos
Patka
Pazmand
Baracska
Soroksari-Duna
Meleg-hegy (352 m) [HILL]
268 m [HILL]
Pettend
Rackeresztur
Szigetcsep
Majoshaza
Nadap
Sukoro
Kapolnasnyek
Ercsi
Velencei-to
Velence
Racszt.peter
Szigetujfalu
Csala
Pogany-ko (241 m) [HILL]
Pakozd
Kisvelence
Szigetszt.marton
Szekesfehervar
Gardony
Aggszt.peter
Szekesfehervar-Oreghegy
Felsobesnyo
Agard
Kiskunlachaza
Sinatelep
Dinnyesi-ferto
Dinnyes
184 m [ROUGH]
Rackeve
Sos-to
Borgond
128 m [ROUGH]
Ivancsa
Szekesfehervar Airfield
Borgond Airfield
Pusztaszabolcs
Zichyujfalu
Adony
Szigetbecse
Seregelyes
Szolohegy
Lorev
Sarviz
Belsobarand
Tac
Makad
Kiskunlachaza Airfield
Szolgaegyhaza
Felsoszentivan
Sarosd Airfield
Kulcs
Csosz
Csillag (159 m) [ROUGH]
Sarosd
Perkata
Aba
Hangos-domb (186 m) [ROUGH]
Racalmas
Soponya
Sarkeresztur
Sarpentele
Dinnyesi Ferto
Alsobesnyopuszta
Laszlohegy 130 m [ROUGH]
Falubattyan
Szabadbattyan
Koszarhegy
Felsoszolgaegyhaza
Potolle
Kisperkata
Alsocikola major
159 m [ROUGH]
Nagycseri puszta
Nagyperkata
Arany-hegy 168 m [ROUGH]
Jakabszallas [FOREST]
Perkatai-hegy 188 m [ROUGH]
Racalmas
Hangospuszta
156 m [ROUGH]
Felsotarnoca
155 m [ROUGH]
Nagylang
Nagyhantos
Dunapentele
Belato-hegy 152 m [ROUGH]
Melykut puszta
Sarszentagota
Nagylok
163 m [ROUGH]
Nagyvenyim
Kislang
150 m [ROUGH]
Kaloz
Felsokortvelyes
Szavics puszta
153 m [ROUGH]
170 m [ROUGH]
Hercegfalva
Egyhaza 160 m [ROUGH]
154 m [ROUGH]
Kislokpuszta
Kokasd
Alsotoborzsok
Felsobaracspuszta
Dunavecse
Nagyhorcsok puszta
Kisapostag
Sarbogard
Szarvaspuszta
Katalinmajor
Apostag
Castle Festetics [FOREST]
Deg
Hatvanpuszta
Annamajor
182 m [ROUGH]
Nagykaracsony
Daruhegy
171 m [ROUGH]
Sarszentmiklos
Lajoskomarom
Kozepbogard
Felsoalap
Solti-Duna
Dunaegyhaza
Szilasbalhas
162 m [ROUGH]
Retszilas
Eloszallas
Solti-halom 124 m [ROUGH]
178 m [ROUGH]
Salancser-hegy 162 m [ROUGH]
Dunafoldvar
Bogard 175 m [ROUGH]
Pusztaegres
Hercegpuszta
Alap
Modrovich puszta
165 m [ROUGH]
Sio
Alsoszentivan
Igar
Saregres
Cece
Nekeresd puszta
Toti puszta
181 m [ROUGH]
Kis-Duna
Ozora
Vampuszta
Simontornya
Hard puszta [FOREST]
Hardi-hegy 210 m [ROUGH]
Dobronte-hegy 220 m [HILL]
220 m [HILL]
Bolcske
208 m [HILL]
195 m [ROUGH]
Kapos
Nemetker
Gyant puszta
Palfa
Vajta
Hatar-hegy 168 m [ROUGH]
68th Guards Rifle Division
233rd Rifle Division
18th Tank Corps
233rd Rifle Division
233rd Rifle Division
233rd Rifle Division
I/25-SS/Hitlerjugend
II/25-SS/Hitlerjugend
III/25-SS/Hitlerjugend
I/26-SS/Hitlerjugend
II/26-SS/Hitlerjugend
III/26-SS/Hitlerjugend
I+II/12-SS/Hitlerjugend
I/1-PzGr/1st Pz.
II/1-PzGr/1st Pz.
I/113-PzGr/1st Pz.
II/113-PzGr/1st Pz.
I/1-Pz/1st Pz.
1-Afk./-/1st Pz.
236th Rifle Division
1st Gds. Mec. Corps
Point 125
I/126/23rd Pz.
II/126/23rd Pz.
I/128/23rd Pz.
II/128/23rd Pz.
128/-/23rd Pz.
128/23rd Pz.
I+II/201/23rd Pz.
33rd Rifle Corps
337th Rifle Division
202nd Rifle Division
206th Rifle Division
33rd Rifle Corps
