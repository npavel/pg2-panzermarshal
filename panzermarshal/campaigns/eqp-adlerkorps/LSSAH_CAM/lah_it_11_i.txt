1st SS-Panzer Division LSSAH, 
SS-Obergruppenfuhrer Joseff Dietrich.

Unternehmen Barbarossa. 
2nd March, 1943. 
Kharkov. 

In October 1942, the LSSAH was transfered to Southern France for a spell of occupation duty.  During this time, the unit was given a refit and was re-designated a Panzergrenadier Division. Its new structure comprises: a tank regiment (2 battalions), two infantry regiments (each 3 battalions), a pioneer battalion, an artillery regiment (3 battalions), an assault battalion, a recon battalion, anti-tank battalion, Flak battalion.  

(If playing with rules, LSSAH has been upgraded to a Panzer Division. Purchase 1 'SS Infanterie', 2 Tanks, 1 Artillery).

In January 1943, the division was transfered back to the Eastern Front, with the situation of Army Group South verging on the collapse.  The encirclement of the 6th Army at Stalingrad was a disaster of the worst kind for Germany.  A vast gap was formed in the line, through which Soviet troops stormed forward, towards Rostov, Kursk and Kharkov.  As Kursk fell on the 8th February, the right flank of Army Group B was forced onto Kharkov. Hitler, remembering the way the Waffen-SS units defended themselves during the winter of 1941 ordered the SS-Panzer Corps to hold Kharkov.  SS-Panzer Corps commander  Paul Hausser disobeyed orders and pulled out of the city.  There they held firm and halted the Soviet attack.  Von Manstein now prepared an assault to destroy the Soviet troops around Kharkov.


