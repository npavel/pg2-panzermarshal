Melitopol counter attack
Welyka Biloserka (Bolshaya Belozerka)
Mala Biloserka (Malaya Belozerka)
Mychajlivka (Mikhaylovka)
Pryshyb (Prishib)
Vynohradne (Alt-Nassau)
Molocna River (Molochnaya)
Tymoshivka (Timoshevka)
Plodorodne (Raykhenfeld)
Starobohdanivka (Bogdanovka)
Piskoshyne (Peskoshino)
Pidhirne (Podgorny)
Novobohdanivka (Novoya Bogdanovka)
Novooleksandrivka (Novaya Aleksandrovka)
Switlodolynske (Likhtenau)
Troitske (Troitskoye)
Menchykury (Menchikur)
Matviivka (Novo Popovka)
Novoivanivka (Novo Ivanovka)
Kurosani River
Vesele (Veseloye)
Spaske (Spasskoye)
Travneve (Altenau)
Zelenij Lug (Zelenyy Lug)
Fedorivka (Fedorovka)
Usanli River
Bilivske (Belovskiy)
Terpinnia (Terpeniye)
Yelyzavetivka (Yelizavetovka)
Trudove
Zakaznik Staroberdyanskyi Forest
Rail Station
Novomykolaivka (Novo-Nikolayevka)
Tasenak River (Tashenak)
Semenivka (Semenovka)
Ozerne (Ozernoye)
Voznesenka
Melitopol
Petrivka (Petrovka)
Novhorodkivka (Novgorodkovka)
Kostiantynivka (Konstantinovka)
Dolynske (Dolinskiy)
Stantsiya Tashchenak (Rail Station)
Druzhbivka (Druzhbovka)
Novoivanivka (Novo-Ivanovka)
Danylo-Ivanivka (Danilo-Ivanovka)
Novopokrovka (Novo-Pokrovskiy)
Hannivka (Annovka)
Mordvynivka (Mordvinovka)
Maksyma Horkoho (Maksim Gorkiy)
Jakymivka (Akimovka)
Dobrivka (Dobrovka)
Velikij Utluk (Bolshoy Utlyuk)
Velyka Ternivka (Bolshaya Ternovka)
Molocnij Liman (Ozero Molochnoye)
I / 6th Rosiori / 5th Cavalry Brigade
II / 6th Rosiori / 5th Cavalry Brigade
I / 7th Rosiori / 5th Cavalry Brigade
II / 7th Rosiori / 5th Cavalry Brigade
I / 8th Rosiori / 5th Cavalry brigade
II / 8th Rosiori / 5th Cavalry brigade
5th Cavalry brigade
II / 2nd / 5th Cavalry brigade
I / 3rd Calarasi / 8th Cavalry brigade
II / 3rd Calarasi / 8th Cavalry brigade
I / 4th Rosiori / 8th Cavalry brigade
II / 4th Rosiori / 8th Cavalry brigade
I / 2nd Calarasi / 8th Cavalry brigade
II / 2nd Calarasi / 8th Cavalry brigade
I / 3rd / 8th Cavalry brigade
II / 3rd / 8th Cavalry brigade
8th Cavalry brigade
13 / 8th / 4th Mountain Brigade
14 / 8th / 4th Mountain Brigade
19 / 8th / 4th Mountain Brigade
17 / 9th / 4th Mountain Brigade
18 / 9th / 4th Mountain Brigade
20 / 9th / 4th Mountain Brigade
3 / 4th / 4th Mountain Brigade
6 / 4th / 4th Mountain Brigade
4th / 4th Mountain Brigade
7 / 4th / 2nd Mountain Brigade
8 / 4th / 2nd Mountain Brigade
15 / 4th / 2nd Mountain Brigade
9 / 5th / 2nd Mountain Brigade
10 / 5th / 2nd Mountain Brigade
16 / 5th / 2nd Mountain Brigade
4 / 2nd / 2nd Mountain brigade
5 / 2nd / 2nd Mountain Brigade
2 / 2nd / 2nd Mountain Brigade
2 / 1st / 1st Mountain Brigade
3 / 1st / 1st Mountain Brigade
23 / 1st / 1st Mountain Brigade
1 / 2nd / 1st Mountain Brigade
4 / 2nd / 1st Mountain Brigade
24 / 2nd / 1st Mountain Brigade
1 / 1st / 1st Mountain Brigade
I / 391 / 170th Infantry Division
II / 391 / 170th Infantry Division
III / 391 / 170th Infantry Division
I / 399 / 170th Infantry Division
II / 399 / 170th Infantry Division
III / 399 / 170th Infantry Division
I / 401 / 170th Infantry Division
II / 401 / 170th Infantry Division
III / 401 / 170th Infantry Division
I / 240 / 170th Infantry Division
II / 240 / 170th Infantry Division
III / 240 / 170th Infantry Division
IV / 240 / 170th Infantry Division
I / 105 / 72th Infantry Division
II / 105 / 72th Infantry Division
III / 105 / 72th Infantrie Division
I / 124 / 72th Infantrie Division
II / 124 / 72th Infantrie Division
III / 124 / 72th Infantrie Division
I / 266 / 72th Infantry Division
II / 266 / 72th Infantry Division
III / 266 / 72th Infantrie Division
I / 706 / 72th Infantry Division
II / 206 / 72th Infantry Division
III / 206 / 72th Infantry Division
IV / 206 / 72th Infantry Division
I / 13th / 4th Mountain Division
II / 13th / 4th Mountain Division
III / 13th / 4th Mountain Division
III / 94th / 4th Mountain Division
IV / 94th / 4th Mountain Division
I / 91th / 4th Mountain Division
II / 91th / 4th Mountain Division
III / 91th / 4th Mountain Division
I / 94th / 4th Mountain Division
II / 94th / 4th Mountain Division
I / 10th Rosiori / 6th Cavalry Brigade
II / 10th Rosiori / 6th Cavalry Brigade
I / 9th Rosiori / 6th Cavalry Brigade
II / 9th Rosiori / 6th Cavalry Brigade
I / 5th Calarasi / 6th Cavalry Brigade
II / 5th Calarasi / 6th Cavalry Brigade
I / 4th / 6th Cavalry Brigade
II / 4th / 6th Cavalry Brigade
LSSAH
1 / 531th / 164th Rifle Division
2 / 531th / 164th Rifle Division
3 / 531th / 164th Rifle Division
1 / 620th / 164th Rifle Division
2 / 620th / 164th Rifle Division
3 / 620th / 164th Rifle Division
1 / 742th / 164th Rifle Division
2 / 742th / 164th Rifle Division
3 / 742th / 164th Rifle Division
531th / 164th Rifle Division
620th / 164th Rifle Division
1 / 494th / 164th Rifle Division
2 / 494th / 164th Rifle Division
1 / 473th / 164th Rifle Division
2 / 473th / 164th Rifle Division
1 / 394th Artillery regiment
2 / 394th Artillery regiment
1 / 371th / 130th Rifle Division
2 / 371th / 130th Rifle Division
3 / 371th / 130th Rifle Division
1 / 528th / 130th Rifle Division
2 / 528th / 130th Rifle Division
3 / 528th / 130th Rifle Division
1 / 664th / 130th Rifle Division
2 / 664th / 130th Rifle Division
3 / 664th / 130th Rifle Division
371th / 130th Rifle Division
528th / 130th Rifle Division
1 / 363th / 130th Rifle Division
2 / 363th / 130th Rifle Division
1 / 268th Artillery regiment
2 / 268th Artillery regiment
1 / 43th / 96th Rifle Division
2 / 43th / 96th Rifle Division
3 / 43th / 96th Rifle Division
1 / 209th / 96th Rifle Division
2 / 209th / 96th Rifle Division
3 / 209th / 96th Rifle Division
1 / 651th / 96th Rifle Division
2 / 651th / 96th Rifle Division
3 / 651th / 96th Rifle Division
43th / 96th Rifle Division
209th / 96th Rifle Division
651th / 96th Rifle Division
1 / 146th / 96th Rifle Division
2 / 146th / 96th Rifle Division
1 / 593th / 96th Rifle Division
2 / 593th / 96th Rifle Division
1 / 35th / 30th Rifle Division
2 / 35th / 30th Rifle Division
3 / 35th / 30th Rifle Division
1 / 71st / 30th Rifle Division
2 / 71st / 30th Rifle Division
3 / 71st / 30th Rifle Division
1 / 256th / 30th Rifle Division
2 / 256th / 30th Rifle Division
3 / 256th / 30th Rifle Division
35th / 30th Rifle Division
71th / 30th Rifle Division
1 / 59th / 30th Rifle Division
147th / 30th Rifle Division
1 / 389th / 176th Rifle Division
2 / 389th / 176th Rifle Division
3 / 389th / 176th Rifle Division
1 / 404th / 176th Rifle Division
2 / 404th / 176th Rifle Division
3 / 404th / 176th Rifle Division
1 / 591st / 176th Rifle Division
2 / 591st / 176th Rifle Division
3 / 591st / 176th Rifle Division
188th / 176th Rifle Division
389th / 176th Rifle Division
1 / 300th / 176th Rifle Division
2 / 300th / 176th Rifle Division
1 / 372th / 218th Rifle Division
2 / 372th / 218th Rifle Division
3 / 372th / 218th Rifle Division
1 / 658th / 218th Rifle Division
2 / 658th / 218th Rifle Division
3 / 658th / 218th Rifle Division
1 / 667th / 218th Rifle Division
3 / 667th / 218th Rifle Division
2 / 667th / 218th Rifle Division
372th / 218th Rifle Division
658th / 218th Rifle Division
667th / 218th Rifle Division
44th / 218th Rifle Division
1 / 287th / 51st Rifle Division
2 / 287th / 51st Rifle Division
3 / 287th / 51st Rifle Division
1 / 348th / 51st Rifle Division
2 / 348th / 51st Rifle Division
3 / 348th / 51st Rifle Division
277th / 51st Rifle Division
63th / 96th Rifle Division
215th / 130th Rifle Division
230th / 164th Rifle Division
73th / 4th Rifle Division
