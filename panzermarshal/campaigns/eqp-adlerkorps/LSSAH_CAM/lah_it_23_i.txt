1st SS-Motorisierte Division LSSAH, 
SS-Obergruppenfuhrer Joseff Dietrich.

Unternehmen Barbarossa. 
28th September, 1941. 
Melitopol Counter Attack. 

In the middle of September, German OKH was confident that the Soviet 9th and 18th armies were almost destroyed and did not pose any threat to German forces in the South sector. General Manstein deployed LSSAH to the Perekop Isthmus intending to create diversions while preparing for the main assault. He also ordered the elite 1st and 4th Mountain divisions to start moving towards the Perekop Isthmus replacing them with Romanian Mountain Corps. These replacements did not remain unnoticed by Soviet intelligence. On the 27th of September, reinforced Soviet 9th and 18th Armies launched a devastating assault aimed at the German southern flank near Melitopol. The attack was so strong attack that it breached the 3rd Romanian Army and Germany's XXX Armeekorps sector of the front threatening an envelopment. 

General Manstein had no option but to turn back the elite mountain divisions and to send his only mobile reserve, LSSAH, to shore up the Romanian defense to halt the attack of Soviet forces. LSSAH is ordered to move as fast as possible to the area of Malaya Beloserka to restore the defense line. After that, move to Veseloye and attack between the 18th and 9th Soviet armies towards Melitopol.

(If playing with rules do not buy any new units, except to replace any core losses.)
