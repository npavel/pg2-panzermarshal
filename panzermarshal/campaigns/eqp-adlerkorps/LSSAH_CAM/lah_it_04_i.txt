Infanterie Regiment (mot) LSSAH, 
SS-Obergruppenfuhrer Joseff Dietrich.

Fall Gelb. 
25th May, 1940. 
Advance on Dunkirk.

After the fighting in Holland, the Leibstandarte was offered a brief rest, during which it remained under the command of the 18th Army.  Shortly after it was transported south and crossed into France on the 20th of May. The regiment soon found itself under the new command of the XXXXIV Corps, 6th Army, Army Group B.  

The SS Leibstandarte and Verfugungs divisions were moved to positions along the Aa canal, which formed part of the defensive perimeter of the trapped BEF. The LSSAH took up a position 15 miles south-west from Dunkirk along the line of Aa Canal facing the Allied defensive line near Watten.

On 25th May, against the Hitler's "Halt" order, Dietrich ordered an assault across the canal to eliminate the artillery positions on the Wattenberg heights that commanded the surrounding flatlands. Suitably appraised of the situation and encouraged by the early success of the advance, Guderian immediately approved the decision taken by Dietrich and ordered the 2nd Panzer Division to move up in support. On 26th May, the whole German army advance resumed...

(If playing with rules do not buy any new units, except to replace any core losses.) 
