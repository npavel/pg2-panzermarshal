Assault on Rostov
Sambek
Tuzlov
Bol'shoi Nesvetai
Malyi Nesvetai
Grushevka
Sovetka
Chistopolye
Voloshino
Aleksandrovka
General'skoye
Oktyabr'skiy
Budennyi
Nesvetai
Kamennyi Brod
Grushevskaya
Novocherkassk
Bol'shie Saly
Aksai
Krivyanskaya
Shchepkin
Bol'shoi Mishkin
Sultan-Saly
Veselyi
Krasnyi Krym
Krym
Bol'shoi Log
Sinyavka
Chaltyr'
Ordzhonikidze
Mertvyi Donets
Kalinin
Rostov
Severnyi
Aksaiskaya
Starocherkasskaya
Don
Lagutnik
Aleksandrovskaya
Arpachin
Krasnyi Gorod-Sad
Sred. Kuter'ma
Nizhnegnilovskoi
Kuter'ma
Bol. Kuter'ma
Ol'ginskaya
Nizhnepodpol'nyi
Rukav Kalancha
Rogozhkino
Mokraya Kalancha
Koisug
Bataisk
Azov
Kuleshovka
Kagal'nik
Khomutovskaya
Krugloye
Peshkovo
Platono-Petrovka
Novonikolayevka
I/4/13th Pz.
II/4/13th Pz.
I/66/13th Pz.
II/66/13th Pz.
I/93/13th Pz.
II/93/13th Pz.
13/-/13th Pz.
I/13/13th Pz.
II/13/13th Pz.
4/-/13th Pz.
I/Germania/Wiking
II/Germania/Wiking
III/Germania/Wiking
I/36/14th Pz.
II/36/14th Pz.
40/-/14th Pz.
I/4/14th Pz.
II/4/14th Pz.
I/103/14th Pz.
II/103/14th Pz.
I/Westland/Wiking
II/Westland/Wiking
III/Westland/Wiking
I/5-SS/Wiking
II/5-SS/Wiking
I/1151/343rd Rifle Division
Rostov Home Guard
353rd Rifle Division
317th Rifle Division
317th Division
230th NKVD Rgt.
31st Rifle Division
I/33/9th Pz.
II/33/9th Pz.
I/10/9th Pz.
II/10/9th Pz.
III/10/9th Pz.
50/-/9th Pz.
9/-/9th Pz.
I/102/9th Pz.
II/102/9th Pz.
III/102/9th Pz.
I/1145/353rd Rifle
II/1145/353rd Rifle
III/1145/353rd Rifle
I/1147/353rd Rifle
II/1147/353rd Rifle
III/1147/353rd Rifle
I/1149/353rd Rifle
II/1149/353rd Rifle
III/1149/353rd Rifle
619/-/353rd Rifle
25/-/353rd Rifle
II/1151/343rd Rifle Division
III/1151/343rd Rifle Division
620/-/343rd Rifle Division
I/1153/343rd Rifle Division
II/1153/343rd Rifle Division
III/1153/343rd Rifle Division
1151/343rd Rifle Division
1151/343rd Rifle Division
1153/343rd Rifle Division
I/903/343rd Rifle Division
II/602/343rd Rifle Division
402/-/343rd Rifle Division
I/1155/343rd Rifle Division 
II/1155/343rd Rifle Division
III/1155/343rd Rifle Division
1151/343rd Rifle Division
48/-/343rd Rifle Division
1153/343rd Rifle Division
1145/353rd Rifle Division
1145/353rd Rifle Division
1147/353rd Rifle Division
I/902/353rd Rifle Division
II/902/353rd Rifle Division
449/-/353rd Rifle Division
401/-/353rd Rifle Division
419/-/343rd Rifle Division
I/571/317th Rifle Division
II/571/317th Rifle Division
III/571/317th Rifle Division
I/606/317th Rifle Division
II/606/317th Rifle Division
III/606/317th Rifle Division
I/773/317th Rifle Division
6th Tank Brigade
48/-/343rd Rifle Division
25/-/353rd Rifle
180/68th Cavalry Division
191/68th Cavalry Division
194/68th Cavalry Division
79/68th Cavalry Division
I/1133/339th Rifle Division
II/1133/339rd Rifle Division
III/1133/339rd Rifle Division
III/13/13th Pz.
43/-/13th Pz.
64/-/14th Pz.
I/108/14th Pz.
II/108/14th Pz.
14/-/14th Pz.
13/-/13th Pz.
4/-/14th Pz.
16/-/16th Pz. Div
16/-/16th Pz.
I/761/317th Rifle Division
II/716/317th Rifle Division
III/716/317th Rifle Division
II/773/317th Rifle Division
606/317th Rifle Division
1153/353rd Rifle Division
III/13/13th Pz.
III/4/14th Pz.
III/4/13th Pz.
III/4/13th Pz.
III/36/14th Pz.
LSSAH
4/-/13th Pz.
13/-/14th Pz.
1/129/31st Rifle Division
1/32/31st Rifle Division
