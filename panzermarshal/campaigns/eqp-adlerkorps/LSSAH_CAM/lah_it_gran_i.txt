1st SS-Panzer Division LSSAH, 
SS-Brigadefuhrer Otto Kumm.

Lake Balaton Offensive. 
February 17th, 1945. 
Operation Sudwind.

By 1945 the Leibstandarte had been successfully moved east by train to Vienna, and the difficult task of rebuilding the division began.  The situation for Germany was critical - Budapest had fallen after a 51 day seige; Warsaw, Lodz and Krakow had fallen; a Soviet Army was in East Prussia; the Red Army was now a mere 200km from Prague, and worst of all, it had crossed the river Oder, and was only 70km from Berlin.  The I SS Panzer Corps was to be used to rectify this critical situation.

The Soviets held a considerable bridgehead over the Gran river, north of Eztergom.  This position was seen as a potential assembly area for a possible thrust towards Vienna and as such it had to be elminated before the main offensive - aimed at elimating the Soviet units from the area west of the Danube and north of the Drava rivers and to secure the Nagykanizsa oil deposits - could begin.  This peliminary operation was given the code-name South Wind.

(If playing with rules do not buy any new units, except to replace any core losses.  There is limited deployment in this scenario, as the bulk of the division had not yet reached the front.)
