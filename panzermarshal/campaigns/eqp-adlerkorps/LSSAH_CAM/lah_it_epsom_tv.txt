By throwing in our last reserves, we were able to achieve a defensive success by containing the British offensive. More than 4,000 casualties were inflicted upon the British. However, the effort cost was more than 3,000 men lost and over 120 German tanks destroyed reducing much our offensive power. 
Even with few infantry divisions coming in, our panzer divisions are forced to remain in the front line rather than pulling back into reserve to recover.

Having had to commit last reserves to contain the British offensive, a permission from Hitler was requested to begin a fighting withdrawal. 
Now, our troops are digging in along the Odon river with larger numbers of new weapon positions.



 
