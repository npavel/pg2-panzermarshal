Charnwood
Gold Beach
River Aure
Juno Beach
Arromanches-les-Bains
Ver-sur-Mer
Courselles-sur-Mer
St. Aubin-sur-Mer
Langrune-sur-Mer
Sword Beach
Lion-sur-Mer
Bayeux
Douvres
Ouistreham
Creully
Thaon
River Orne
Martragny
Carpiquet
Libisey
Ranville
Bretteville-l'Orgueilleuse
Caen
Colombelles
Tilly-sur-Suelles
Toufreville
Caen Airport
River Oden
Cagny
Bourguebus Ridge
Soliers
Bras
Vimont
Hubert-Folie
Evrecy
Vieux
Bourguebus
Villers-Bocage
River Ajon
May-sur-Orne
Fresney
II/12-SS/Hitlerjugend
II/12-SS-Pz/Hitlerjugend
I/25-SS/Hitlerjugend
II/25-SS/Hitlerjugend
III/25-SS/Hitlerjugend
I/26-SS/Hitlerjugend
II/26-SS/Hitlerjugend
III/26-SS/Hitlerjugend
12-SS-Aufk./-/Hitlerjugend
12-SS-Pio./-/Hitlerjugend
I/12-SS-Aty/Hitlerjugend
II/12-SS-Aty/Hitlerjugend
12-SS-Flk./-/Hitlerjugend
PzJg. Lehr/-/Lehr
130/-/Lehr
I/901/Lehr
II/901/Lehr
I/902/Lehr
II/902/Lehr
I/6/3rd Pz. [Lehr]
II/130/Lehr
Pio. Lehr/-/Lehr
I/130-Aty./Lehr
II/130-Aty./Lehr
RWR/7/3rd Infantry
RR/7/3rd Infantry
CS/7/3rd Infantry
NSR/8/3rd Infantry
QOR/8/3rd Infantry
LRC/8/3rd Infantry
HLI/9/3rd Infantry
SD/9/3rd Infantry
GH/9/3rd Infantry
I+II/10-SS/Frundesberg
II/10-SS/Frundesberg
I/21-SS/Frundesberg
II/21-SS/Frundesberg
III/21-SS/Frundesberg
I/22-SS/Frundesberg
II/22-SS/Frundesberg
III/22-SS/Frundesberg
10/-/Frundesberg
I/21-SS/Frundsberg
II/21-SS/Frundsberg
III/21-SS/Frundsberg
I/22-SS/Frundsberg
II/22-SS/Frundsberg
III/22-SS/Frundsberg
I+II/10-SS/Frundsberg
II/9-SS/Hohenstaufen
III/9-SS/Hohenstaufen
IV/9-SS/Hohenstaufen
Das Reich BG
4L/146/49th Infantry
4KOYL/146/49th Infantry
H/146/49th Infantry
5WY/147/49th Infantry
6DWR/147/49th Infantry
6L/148/49th Infantry
7DWR/147/49th Infantry
1F/148/49th Infantry
8F/148/49th Infantry
69/-/49th Infantry
70/-/49th Infantry
71/-/49th Infantry
58/-/49th Infantry
8RS/44/15th Infantry
6KOSB/44/15th Infantry
7KOSB/44/15th Infantry
6RSF/45/15th Infantry
9C/45/15th Infantry
10C/45/15th Infantry
10HLI/46/15th Infantry
11HLI/46/15th Infantry
2GH/46/15th Infantry
129/-/15th Infantry
130/-/15th Infantry
131/-/15th Infantry
64/-/15th Infantry
1H/128/43rd Infantry
2H/128/43rd Infantry
5H/128/43rd Infantry
4SLI/129/43rd Infantry
4W/129/43rd Infantry
5W/129/43rd Infantry
7H/130/43rd Infantry
4D/130/43rd Infantry
5D/130/43rd Infantry
94/-/43rd Infantry
112/-/43rd Infantry
141/-/43rd Infantry
59/-/43rd Infantry
RWR/7/3rd Infantry
RRR/7/3rd Infantry
CSR/7/3rd Infantry
QOR/8/3rd Infantry
LDC/8/3rd Infantry
NSR/8/3rd Infantry
HLI/9/3rd Infantry
SDG/9/3rd Infantry
NNH/9/3rd Infantry
I/1.SS-Pz/LSSAH
I/1.SS/LSSAH
II/1.SS/LSSAH
III/1.SS/LSSAH
I/1.SS-Aty/LSSAH
23H/29/11th Arm.
3RTR/29/11th Arm.
FFY/29/11th Arm.
POC/29/11th Arm.
4KSLI/159/11th Arm.
3MR/159/11th Arm.
1HR/159/11th Arm.
3rd Infantry Division
43rd Infantry Division
15th Infantry Division
RAF
[2nd Army]
I/31/16th Luftwaffe-Feld
II/31/16th Luftwaffe-Feld
III/31/16th Luftwaffe-Feld
I/32/16th Luftwaffe-Feld
II/32/16th Luftwaffe-Feld
III/32/16th Luftwaffe-Feld
I/46/16th Luftwaffe-Feld
II/46/16th Luftwaffe-Feld
III/46/16th Luftwaffe-Feld
I+II/16/16th Luftwaffe-Feld
1SR/8/3rd Infantry Div.
2EYR/8/3rd Infantry Div.
1SL/8/3rd Infantry Div.
2LR/9/3rd Infantry Div.
1KOSB/9/3rd Infantry Div.
2RUR/9/3rd Infantry Div.
2RWR/185/3rd Infantry Div.
1RN/185/3rd Infantry Div.
2KSLI/185/3rd Infantry Div.
4LR/146/49th Infantry Div.
Luftwaffe
2EYR/8/3rd Infantry Div.
1SR/8/3rd Infantry Div.
1SL/8/3rd Infantry Div.
1KOSB/9/3rd Infantry Div.
2LR/9/3rd Infantry Div.
2RUR/9/3rd Infantry Div.
1RN/185/3rd Infantry Div.
2RWR/185/3rd Infantry Div.
2KSLI/185/3rd Infantry Div.
4L/146/49th Infantry
4KOYL/146/49th Infantry
H/146/49th Infantry
6DWR/147/49th Infantry
5WY/147/49th Infantry
7DWR/147/49th Infantry
1F/148/49th Infantry
8F/148/49th Infantry
6L/148/49th Infantry
