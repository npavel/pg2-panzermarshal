Infanterie Regiment (mot) LSSAH, 
SS-Obergruppenfuhrer Joseff Dietrich.

Fall Gelb. 
9th May, 1940. 
Attack into Holland.

In early 1940, Leibstandarte was expanded into a full independent motorized infantry regiment comprising: 3 infantry battalions; 1 pioneer battalion; 1 artillery regiment (2 battalions); 1 reconnaissance battalion. 

(NOTE: If playing with rules, you should purchase one '10.5mm leFH18' artillery unit)

The regiment was shifted to the Dutch border for the launch of Fall Gelb. The German assault in the west finally began before daylight on May 9th with extensive air attacks on Dutch and Belgian airfields and the seizure of vital river crossings by paratroops.  When dawn came, the Leibstandarte, as part of 18th Army, Army Group B, drove into Holland, with its main column striking towards the southern Netherlands to envelop the southern flank of the 'Vesting Holland' region.

This time the Leibstandarte is attached to the 227th Infantry Division and has been given the role of penetrating the enemy defences and securing the road and river bridges. Our first target is the Ijssel river and city Arnhem. 

