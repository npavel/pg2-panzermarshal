French 7th Army drive North May 1940<br>

The French 7th Army has crossed the Belgian-Dutch border. Their 1st Light Mechanized Division is rushing to Moerdijk. If they capture the Moerdijk bridge, your forces are trapped North of the rivers. Hurry your SS forces and the 9th Pz Division South to push the frogs back into Belgium.

