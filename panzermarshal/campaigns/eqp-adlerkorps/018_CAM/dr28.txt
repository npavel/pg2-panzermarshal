Wacht am Rhein - December 1944<br>

Lines have stabilized after your retreat from Normandy. Hitler has gathered all available forces for a counterattack. Your division and the 9thSS Hohenstaufen form the 2SS Pz Corps. The original plan called for you to exploit the initial success of the 1SS Pz Corps attack on the northern shoulder of our attack through the Ardennes. The enemy however has stalled the LSSAH's attack. The goal of advancing as far west as Antwerp and cutting the Allied forces in half has been given up. 
Your troops have been transferred from 6SSPz Armee to the 5PzArmee and have swung south around LSSAH. Your task is to capture the crossroads at Baraque Fraiture, push through the US lines of defense and capture Liege. Maybe that will move the Allies to start peace negotiations.

