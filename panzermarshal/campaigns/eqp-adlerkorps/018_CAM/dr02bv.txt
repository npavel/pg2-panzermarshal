Student May1940 Brilliant Victory

Student's troops did such a great job, capturing the Dutch government and Queen Wilhelmina that the Dutch capitulated before the 9th Panzer division and your troops came to the scene. 

HISTORY
After 4 days of fighting, Goering decided to force the Dutch in Rotterdam to surrender by bombing the city. When Generalleutnant Schmidt (9th PzDiv) reached an agreement on the terms of surrender of Rotterdam with it's Dutch commander, it was too late to recall all bombers and Rotterdam city center got hit very hard. The same day, Dutch General Winkelman decided to have all equipment destroyed in preparation for the overall Dutch surrender.
On that May 14th 1940 General Student was hit in the head by a bullet from an SS sniper in Rotterdam. You and your SS friends are very lucky he survived.
