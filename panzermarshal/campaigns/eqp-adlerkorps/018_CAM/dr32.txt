Vienna - April 1945<br>

The Russians have almost completely encircled Vienna. Your division's task is too hold open the bridges over the Danube to let as many troops as possible escape to the west.

