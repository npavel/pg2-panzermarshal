Student May1940<br>

With the Polish problem solved, we are now looking at the West: The Netherlands, Belgium and France.
We had to abandon our original plan 'Fall Gelb' after one of our Fallschirmguys lost the plans to the Belgians when his plane crashed. The new Sichelschnitt plan is much more daring, shifting the focus of the attack from The Netherlands and Belgium to the South of Belgium: the supposedly impassable Ardennes. This leaves the forces that need to capture the Netherlands much weaker than originally planned. Generalleutnant Student will land his FliegerKorps XI Fliegerdivision 7 and Luftlandedivision 22 near The Hague, Rotterdam and the bridges at Dordrecht and Moerdijk. Your task is to follow Generalleutnant Schmidt's 9th Panzerdivision through the South of the Netherlands. Once your forces arrive as West as Moerdijk, turn North to the Hague and follow the path cleared by Student's troops.

