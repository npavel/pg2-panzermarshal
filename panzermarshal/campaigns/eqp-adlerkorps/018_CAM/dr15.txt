Operation Typhoon - Moscow Novembe r1941<br>

We had to break up our 11SS Regiment to bring the Deutschland and Der Fuerher regiments up to strength again. Some of our units have started to run out of ammo. But even though the  mud, rain and cold slowed us down, we have come close to the end goal of our offensive: the capture of Moscow. The 10th Pz Division will again be fighting at our left flank. Make us proud General.

