Smolensk-Crossing the Dnepr July 1941<br>
Your Das Reich division is part of Guderian's 2nd Panzer Gruppe. Your troops have covered more than 700km in a bit over 2 weeks since Operation Barbarossa started but have not seen much action yet.
Guderian has decided to not wait for the infantry divisions and to cross the Dnepr with only his mobile troops because General Hoth's 3rd Panzer Gruppe has already done the same to the north of us. Both Gruppen could meet at Smolensk thereby trapping more Russians in a new pocket. 10Pz Division will cross the Dnepr North of Mogilev with your division and the GrossDeutschland Infantry Regiment covering the flanks. The first goals are Kopyr and Gorki.

PLAYER ORDERS:
- Capture all Victory Hexes.
