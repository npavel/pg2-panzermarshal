Maginot line garrison June1940 TV

France surrenders! We accomplished in 6 weeks what could not be done in 4 years 25 years earlier. German troops will parade on the Champs Elysees.
Because of your good performance in the Western campaign, you get control over the complete SS-V Division - Standarte Der Fuehrer, Standarte Deutschland, Standarte Germania and their supporting units.

PLAYER ORDERS:
- If playing with restrictions: You can expand your core with 6 Infantry Battalions, a Pioniere Battalion and an Anti Tank (AT) Battalion to: 1 FlaK, 3 Aty, 9 Inf, 1 Pio, 1 Recon, 1 Towed AT.

