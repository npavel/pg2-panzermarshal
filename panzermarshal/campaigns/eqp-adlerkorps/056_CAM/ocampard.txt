Ardennes
Butgenbach
Dom Butgenbach
Bullingen
Berg
Niarum
Elsenborn
Camp Elsenborn
Wirtzfeld
Rocherath
Krinkelt
Weywertz
Robertville
Malmedy
Village
Ambleve River
Waimes
Faymonville
Schoppen
Moderscheid
Honsfeld
Murringen
Losheimergraben
Buchholz Sto
Neuhof
Losheim
Eibertingen
12th SS Pz Div. HQ
1/I/25/12th SS Pz Div.
2/I/25/12th SS Pz Div.
12/-/12th SS Pz Div.
I/12/12th SS Pz Div.
1/I/27/12th Inf. Div.
2/I/27/12th Inf. Div.
12/-/12th Inf. Div.
1/II/25/12th SS Pz Div.
2/II/25/12th SS Pz Div.
1/II/12/12th SS Pz Div.
2/II/12/12th SS Pz Div.
1/I/26/12th SS Pz Div.
1/II/26/12th SS Pz Div.
2/I/26/12th SS Pz Div.
2/II/26/12th SS Pz Div.
II/12/12th SS Pz Div.
I/8/3rd PzG Div.
II/8/3rd PzG Div.
III/8/3rd PzG Div.
277th Inf. Div. HQ
I/989/277th Inf. Div.
II/989/277th Inf. Div.
I/990/277th Inf. Div.
II/990/277th Inf. Div.
I/991/277th Inf. Div.
II/991/277th Inf. Div.
I SS Pz Corps HQ
II/48/12th Inf. Div.
1/II/27/12th Inf. Div.
2/II/27/12th Inf. Div.
1/I/5/3rd FJ Div.
II/5/3rd FJ Div.
III/5/3rd FJ Div.
I/8/3rd FJ Div.
II/8/3rd FJ Div.
1/III/8/3rd FJ Div.
Mtr/5/3rd FJ Div.
Mtr/8/3rd FJ Div.
I/3/3rd FJ Div.
II/3/3rd FJ Div.
2/I/5/3rd FJ Div.
2/III/8/3rd FJ Div.
I/38/2nd Inf. Div.
II/38/2nd Inf. Div.
III/38/2nd Inf. Div.
I/393/99th Inf. Div.
II/393/99th Inf. Div.
III/393/99th Inf. Div.
I/394/99th Inf. Div.
II/394/99th Inf. Div.
370/-/99th Inf. Div.
371/-/99th Inf. Div.
38/-/2nd Inf. Div.
I/23/2nd Inf. Div.
I/120/30th Inf. Div.
II/120/30th Inf. Div.
III/120/30th Inf. Div.
15/-/2nd Inf. Div.
I/9/2nd Inf. Div.
II/9/2nd Inf. Div.
III/9/2nd Inf. Div.
Forward HQ 3rd FJ Div.
II/23/2nd Inf. Div.
III/23/2nd Inf. Div.
37/-/2nd Inf. Div.
12th SS Pz Div. Field HQ
