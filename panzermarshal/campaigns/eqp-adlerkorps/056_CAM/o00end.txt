Oran has been captured with only light casualties although the French offered stronger resistance than expected.  Your men proudly stood their ground in their first battle of the war.  Currently there are very few American divisions fighting in this war but so far the US Army has shown its potential.

Prepare to move east into Tunisia.  The French have asked for a cease fire so our only enemy left is Rommel and his veteran Afrika Korps!
