Aachen
Wurm River
Inde River
Vichtbach River
Westwall
Kohlscheid
Laurensberg
Bardenberg
Birk
Euchen
Mariadorf
Stolberg
Eilendorf
Rothe Erde
Haaren
Verlautenheide
Wuerselen
Aachen
Lousberg: HILL
Salvatorberg: HILL
1106th Eng Combat Grp [VII]
I/352/246th Inf. Div.
II/352/246th Inf. Div.
I/689/246th Inf. Div.
II/689/246th Inf. Div.
III/689/246th Inf. Div.
I/246/246th Inf. Div.
II/246/246th Inf. Div.
III/246/246th Inf. Div.
108th Pz Brigade [LXXXI]
506th sPz Abt [LXXXI]
KG Fritzchen
StuG Brigade 394 [LXXXI]
StuG Brigade 341 [LXXXI]
1104th Eng Combat Grp [XIX]
Crucifix Hill (Hill 239)
Ravels Hill (Hill 231)
Luftwaffen-Festungs-Bn 18
Festungs-MG-Bn 34
Reserve-Grenadier-Bn 453
Volkssturm-Bataillon Trier
I/29/3rd PzG Div.
II/29/3rd PzG Div.
I/8/3rd PzG Div.
II/8/3rd PzG Div.
USAAF 492 Squadron
I/120/30th Inf. Div.
II/120/30th Inf. Div.
III/120/30th Inf. Div.
I/404/246th Inf. Div.
II/404/246th Inf. Div.
I/148/49th Inf. Div.
II/148/49th Inf. Div.
I/60/116th Pz Div.
II/60/116th Pz Div.
KG Diefenthal
II/156/116th Pz Div.
I/156/116th Pz Div.
116/-/116th Pz Div.
I/129/29th Inf. Div.
II/129/29th Inf. Div.
III/129/29th Inf. Div.
99th Infantry Bn [XIX]
675/-/116th Pz Div.
2nd Landesschutzen Bn
SS-Battalion Rink
TF Hogan (3rd Arm. Div.)
FlaK Regiment Aachen
Kohlscheid Volksturm
III/352/246th Inf. Div.
3rd Armored Division
LXXXI Corps Rearguard
224/-/29th Inf. Div.
197/-/30th Inf. Div.
957th FA [VII]
212th FA Brigade [VII]
USAAF 493 Squadron
USAAF 641 Squadron
II/146/116th Pz Div.
281/-/116th Pz Div.
I/16/116th Pz Div.
3/-/3rd PzG Div.
Mtr/8/3rd PzG Div.
