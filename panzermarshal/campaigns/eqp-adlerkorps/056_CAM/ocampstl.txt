Operation Cobra
Portail
St Lo d'Orville
Baupte
St Souveur le Vicomte
St Souveur de Pierre Pont
La Haye du Puits
Lessay
Periers
St Souveur Landelin
Marigny
Berigny
Airel
Trevieres
Longueville
Vierville sur Mer
Isigny
Grandcamp les Bains
Coutances
Torigni-sur-Vire
Tessay-sur-Vire
CCB/2nd Arm. Div.
CCB/3rd Arm. Div.
CCA/3rd Arm. Div.
3rd Arm. Div. Infantry
CCA/2nd Arm. Div.
3rd Arm. Div. Artillery
CCA/4th Arm. Div.
CCB/4th Arm. Div.
CCR/4th Arm. Div.
4A
4A
CCA/6th Arm. Div.
CCR/6th Arm. Div.
CCB/6th Arm. Div.
6th Arm. Div. Infantry
6th Arm. Div. Artillery
9/2nd Inf. Div.
23/2nd Inf. Div.
38/2nd Inf. Div.
741/2
11/5th Inf. Div.
2
2/5th Inf. Div.
10/5th Inf. Div.
735/5
818/-/5th Inf. Div.
5th Inf. Div. Artillery
13/8th Inf. Div.
28/8th Inf. Div.
709/8
121/8th Inf. Div.
8th Inf. Div. Artillery
39/9th Inf. Div.
47/9th Inf. Div.
9
899/9
117/30th Inf. Div.
119/30th Inf. Div.
823/30
30th Inf. Div. Artillery
134/35th Inf. Div.
137/35th Inf. Div.
320/35th Inf. Div.
737/35
654/-/35th Inf. Div.
821/35
35th Inf. Div. Artillery
313/79th Inf. Div.
314/79th Inf. Div.
314/79th Inf. Div.
79th Inf. Div. Artillery
329/83rd Inf. Div.
330/83rd Inf. Div.
802/83
83
357/90th Inf. Div.
358/90th Inf. Div.
712/90
359/90th Inf. Div.
90th Inf. Div. Artillery
2/2SS
3 "Deu"/2nd SS Pz Div.
4 "DF"/2nd SS Pz Div.
2/2nd SS Pz Div.
2SS
17
37/17
17
Pz Lehr Div. Remnants
902/L
L
9th FJ/3rd FJ Div.
8th FJ/3rd FJ Div.
3/3rd FJ Div.
5th FJ/3rd FJ Div.
14/5
5
6th FJ Rgt [LXXXIV]
243rd Inf. Div. Remnants
77th Inf. Div. Remnants
895/265
896/265
KG Heinz (266th ID)
899/266
91st Inf. Div. Remnants
266
983/275
984/275
275
343rd Inf. Div. Remnants
353rd Inf. Div. (-)
343
275th Inf. Div. (-)
352
941/353
353
Carentan
St Lo
St Lo
St. Lo
Villers Fossard
St Jean de Day
Sainteny
Taute River
Douve River
Merderet River
Vire River
Aure River
265th Inf. Div. KG
17th SS PzG Div. Remnants
2nd Pz Div. Elements
353rd Inf. Div.
Foret de Cerisy: FOREST
4th Arm. Div. Infantry
4th Arm. Div. Artillery
2nd Inf. Div. Artillery
83rd Inf. Div. Artillery
2nd Arm. Div. Infantry
2nd Pz Div. Forward Elements
[LXXXIV]
USAAF 107 Squadron
USAAF 109 Squadron
USAAF 640 Squadron
USAAF 641 Squadron
