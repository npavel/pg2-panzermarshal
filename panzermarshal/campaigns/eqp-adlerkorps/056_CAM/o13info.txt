ROER CROSSING (8 February 1945)

The German Army is in retreat and we will give it no respite.  The counteroffensive against the bulge will be translated in a huge Allied offensive along the entire front to reach the Rhine River.

Your division will have to break through the West wall for a second time, this time near Schmidt.  The US 2nd Infantry Division is on your right flank.  Together you must break through and force your way over the Roer River.  This is very important because of the many dams here.  If the Germans break the dams, the Roer River valley will be flooded.  US Ninth Army to the north is waiting for you the capture of these dams before crossing the Roer River and advancing towards the Rhine River.

The German defenses are expected to be in bad shape after the losses suffered during the Ardennes campaign but do not underestime the enemy.  Also you are again advancing through pieces of the dreaded Huertgen Forest.

IF PLAYING WITH RULES: Do not upgrade any units except recon and anti-tank units. Your tank may now also be upgraded to a 2-range tank. Your core should be 4 ARTY (1 155m M1, 3 105mm M2), 9 INF (44 Infantry), 1 RECON, 1 AT, 1 TANK (range 2).