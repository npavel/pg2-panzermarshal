TROINA (31 JULY 1943)

After a quick initial advance, both the British Eight and the US Seventh Army are bogged down at the so-called Etna Line.  This is a defensive line which makes full use of the excellent defensive terrain.  Even more, German veterans are manning this line.  However, Messina is still our main goal.  The Big Red One will pierce through the Etna Line by taking Troina and opening the inland road to Messina.

Troina is one of the key points of the Etna Line and is heavily defended by German panzergrenadiers.  Taking such a position seems a typical task for the veteran Big Red One.  You will be supported by Long Tom batteries and a battalion of North African Goums put under your command.

IF PLAYING WITH RULES: Do not upgrade any units except recon and anti-tank units. Your core should be 4 ARTY (1 155m M1, 3 105mm M2), 9 INF (43 Infantry), 1 RECON, 1 AT. 