The hills have been taken and armoured forces are driving towards Bizerte.  It will be only a matter of days before the enemy is forced to surrender!  The role of the Big Red One in the North African campaign is one to be proud of.

This World War is far fromt over.  Hold your division in good shape because I have no doubt that this veteran division will soon be called upon for further glory!