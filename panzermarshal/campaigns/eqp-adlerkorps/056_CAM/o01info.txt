EL GUETTAR (21 MARCH 1943)

What should have been an easy victory has turned into a nightmare.  The Axis have captured the Tunisian ports and brought in plenty of troops, supplies and other war material.  The Tunisian campaign will be a hard fight.  On top of that the pride of the US Army has been damaged by Rommel's attack and victory near Kasserine, but the time has come to strike back and turn the tide!

Montgomery is keeping the Afrika Korps busy at the Mareth Line, and the French and other British forces are binding Axis forces in central and northern Tunisia.  The US II Corps will attack in southern Tunisia near El Guettar. 

Italian troops have been digging in for weeks so breaching the first positions will be hard. Be wary because El Guettar is close to the Afrika Korps positions at the Mareth Line and it is not unthinkable that Rommel could send Panzers to stop a breakthrough.  However, the pride of the US Army is at stake.  Show the world that we are capable of waging war and that we don't take second place to the British!

IF PLAYING WITH RULES:  Do not upgrade any units except recon and anti-tank units. Your core should be 4 ARTY (1 155m M1, 3 105mm M2), 9 INF (42 Infantry), 1 RECON, 1 AT. 
