    On the Morava river : Kromeriz - Kojetin -  2 May 1945
   
   The last days of the war found romanian forces (1st and 4th Armies) active in offensives in Czechoslovakia.
   Important center on the Morava river, Kromeriz was part of a strong defensive area organised by 182nd and 271st german infantry divisions. Romanian 1st Army had the mission to advance north on the Morava river and eliminate all german resistance.  
