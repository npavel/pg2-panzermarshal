  9 deploy hexes in the next scenario
___________________________________

'Vanatorul de care R-35' (R-35 tank destroyer) 

    After the disastrous results of the battle of Stalingrad, suggestions were made to modernise the existing R-35 tanks, either by replacing the original turret with the one of the Soviet T-26 light tank or replacing the main gun with a Soviet 45mm or Schneider 47mm anti-tank gun. In December 1942 it has been decided that the research should focus on replacing the original 37mm gun with the Soviet 45mm gun salvaged from captured BT-7 and T-26 tanks.
    The prototype was ready at the end of February 1943 and after it was tested in the summer of 1943, the Mechanised Troops Command ordered the conversion of thirty R-35 tanks.
    The R-35 tank destroyers were used together with the R-35 tanks in the campaign in Czechoslovakia and Austria, all being lost by the end of the war.

(from www.worldwar2.ro)  