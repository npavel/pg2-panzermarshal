
The operations at Odessa highlighted significant weaknesses in the Romanian Army, leading both military and political leaders in the country to call for a discontinuation of military operations against the Soviet Union.Antonescu ignored such objections, regarding continued participation and eventual victory on the Eastern Front as necessary for the restoration of Romania's territorial integrity.

The Romanian victory at Odessa represented the most significant victory ever achieved independently by an Axis satellite.

The recapture of Bessarabia and Northern Bukovina in Operation M�nchen and the subsequent victory at Odessa led to a partial demobilization of the Romanian army, which was reduced in size from nearly 900,000 personnel on 1 October to 465,000 on 1 January 1942. Politically, the presence of Romanian troops in Odessa and the establishment of the Transnistria Governorate led to a deterioration of Romania's international situation, with the British declaring war on Romania on 6 December, and on 12 December Romania declared war on the United States in solidarity with Germany and Japan.

NMS Delfinul

At the beginning of the war (22 June 1941), the Marina Regala Romana (Romanian Royal Navy) had only one submarine: the NMS Delfinul. It had been built in Italian shipyards, at Fiume, and had been launched on 5 May 1936.

On the other side, the Soviet ChF (Chernomorsky Flot = Black Sea Fleet) had 47 submarines. But the existence of this one Romanian submarine forced the Soviet fleet divert a lot of resources to anti-submarine duties in the first two years of war. For this reason it was used very carefully, since its loss was almost irreplaceable in that period. 

It was confiscated by the Soviets after 23 August 1944, but was returned to Romania after a while, damaged. It was the most successful Romanian submarine in WWII.

(article from www.worldwar2.ro)