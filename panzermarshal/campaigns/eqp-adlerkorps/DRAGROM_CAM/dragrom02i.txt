   Siege of Odessa  18 August 1941

   The siege was primarily conducted by Romanian forces and elements of the German Army's (Wehrmacht Heer) 11th Army (11. Armee). Due to the heavy resistance of the 9th Independent Army (initially) and the rapidly formed Separate Coastal Army, which was formed from the Coastal Group of the 9th Army[1]), and the Black Sea Fleet forces in Odessa, it took the Romanian army 73 days of siege and four attempts to take the city, during which they suffered 93,000 casualties.

On 14 October 1941, most of the remaining forces of the Red Army were evacuated.
