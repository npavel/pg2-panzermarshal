
    AB armored cars

    On 12 December 1942 the Reconnaissance Group of the 1st Armored Division received a company of German armored cars SdKfz 222 (10 pieces), with the designation AB.
     In September 1943, the German Army offered as logistic support to Romanian Army eight AB 41 armored cars, former belonging to the Italian expeditionary corps in Russia. 
More armored cars were imported from Germany as part of the armament delivery program that took place between November 1943 - August 1944. 
     At the moment of the Soviet offensive of 19 August 1944 on Moldavian front, the 1st Armored Division had 12 armored cars. 
     The 2nd Tank Regiment, the last formation of the 1st Armored Division operational at the end of the war, had one reconnaissance company made of eight armored cars and five SPWs.  

(article from www.worldwar2.ro)