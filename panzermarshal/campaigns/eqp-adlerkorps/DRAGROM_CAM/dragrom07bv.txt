    T-38 light tank

    Between May and June 1943 Germany delivered to Romanian troops in Kuban fifty worn Panzerkampfwagen 38(t). 
     The tanks were little superior to R-2 and still vulnerable to all Soviet anti-tank guns and AT rifles. They received designation "T-38" and formed the T-38 Tank Battalion in the organic of the 2nd Tank Regiment, with the 51st, 52nd and 53rd companies of fifteen T-38s each.
     They took part in the defensive battles of Kuban and Crimea.  Starting from November 1943 the T-38s of the 51st and 52nd companies were evacuated to Romania. However, in April 1944 there were still ten T-38s of the 53rd Tank Company in support of the 10th Infantry Division in Crimea. Many have been lost in these operations and in August 1944 the 2nd Tank Regiment could hardly set up a company of 9 T-38 tanks. They participated in the fights around Bucharest and at Ploiesti oil fields, and from March 1945 in the forcing of rivers Hron, Nitra, Vah, Morava in Czechoslovakia and then into Austria.

(from www.worldwar2.ro)  