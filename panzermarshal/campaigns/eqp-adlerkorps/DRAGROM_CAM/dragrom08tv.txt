Partisans proved to be quite a problem. The 1st Mountain Brigade also fought them between 6 and 18 november, before it was sent to take part in the assault on Sevastopol, but did not manage to wipe them out. However, by 15 December, the 4th mountain brigade managed to secure most of communication routes and to destroy the partisan nests, despite the terrible winter conditions.

     TAs assault gun 

     Between November 1943 and August 1944 Germany delivered 108 Sturmgeschutz III Ausf G that were distributed to the 1st Armoured Division, the 8th Motorised Cavalry Division and the Armoured Detachment of the 4th Army, under the designation "TAs" (Tun de Asalt, Assault Gun). 
     The assault guns of the 1st Armoured Division saw action during the battle of Moldavia and the Iassy-Khisinev operation, but those of the 8th Cavalry never saw action as they were seized by the Germans on 22 August and formed the Braun Armoured Detachment. Most of the armoured vehicles of the 1st Armoured Division, including assault guns, were captured by the Soviets during 23-28 August, in spite of the fact that Romanian troops have ceased hostilities. The remaining TAs were used against Germans in the liberation of Romanian territory and with the 2nd Tank Regiment in the operations in Czechoslovakia and Austria. 

(from www.worldwar2.ro)  