BRILLIANT VICTORY

Superlative work, General.  We have the Hun on the run, and the road to Paris is gapingly wide open.  Lovely French girls and croissants await the sex-starved and salivating Allied Armies.  Unfortunately, we have orders to march into Belgium instead.  So, Belgian girls and waffles await us.  If we're lucky.