BRILLIANT VICTORY

Excellent performance, General.  It was a bit hairy for some time on the island, as we traversed its difficult landscape against a determined opponent.  Fatigue and the degradation of the men's morale was a worry throughout, illustrated at the point where some men started sticking pencils up their nose and putting underpants on their head - claiming to be mad.....oh wait, that was just me...

Ahem.  Anyway, our work on Madagascar is done and the island is secured.  A new assignment awaits us.
