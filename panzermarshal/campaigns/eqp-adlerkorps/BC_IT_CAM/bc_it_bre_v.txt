VICTORY

Nice work, General.  Thanks to our attacks here, and the Canadian assault to the west, we've finally cleared the Scheldt estuary and can now use the port of Antwerp, bringing in much needed supplies and reinforcements.  Unfortunately, the Hun isn't playing fair, and has mined the entire area, meaning it will take weeks to clear for our shipping.