Operation Goodwood
Juno Beach
Villiers-sur-Mer
Sword Beach
Langrune-sur-Mer
River Dives
Lion-sur-Mer
Dives
Douvres
Ouistreham
Cabourg
Merville
River Orne
Dozule
Carpiquet
Ranville
Lebisey
Caen
Colombelles
Toufreville
Troarn
River Odon
Cagny
Bourguebus Ridge
Soliers
River Touques
Bras
Vimont
Hubert-Folie
Moult
Bourguebus
May-sur-Orne
Mezidon
Fresney
Bretteville-sur-Laize
St. Pierre-sur-Dives
Gold Beach
River Aure
Arromanches-les-Bains
Ver-sur-Mer
Courselles-sur-Mer
St. Aubin-sur-Mer
Bayeux
Creully
Thaon
Martragny
Libisey
Bretteville-l'Orgueilleuse
Tilly-sur-Suelles
Caen Airport
River Oden
Evrecy
Vieux
Villers-Bocage
River Ajon
I/980/272nd Infantry
II/980/272nd Infantry
III/980/272nd Infantry
I/981/272nd Infantry
II/981/272nd Infantry
III/981/272nd Infantry
I/982/272nd Infantry
II/982/272nd Infantry
III/982/272nd Infantry
I/272/272nd Infantry
II/272/272nd Infantry
III/272/272nd Infantry
IV/272/272nd Infantry
272/-/272nd Infantry
I/731/711th Infantry
II/731/711th Infantry
III/731/711th Infantry
I/744/711th Infantry
II/744/711th Infantry
III/744/711th Infantry
I/651/711th Infantry
II/651/711th Infantry
I/31/16th Luftwaffe-Feld
II/31/16th Luftwaffe-Feld
I/32/16th Luftwaffe-Feld
I+II/10-SS/Frundesberg
II/10-SS/Frundesberg
I/21-SS/Frundesberg
II/21-SS/Frundesberg
III/21-SS/Frundesberg
I/22-SS/Frundesberg
II/22-SS/Frundesberg
III/22-SS/Frundesberg
10/-/Frundesberg
I/19-SS/Hohenstaufen
II/19-SS/Hohenstaufen
III/19-SS/Hohenstaufen
I/20-SS/Hohenstaufen
II/20-SS/Hohenstaufen
III/20-SS/Hohenstaufen
I+II/9-SS/Hohenstaufen
II/9-SS/Hohenstaufen
III/9-SS/Hohenstaufen
IV/9-SS/Hohenstaufen
9/-/Hohenstaufen
I/130/Panzer-Lehr
II/130/Panzer-Lehr
I/901/Panzer-Lehr
II/901/Panzer-Lehr
III/901/Panzer-Lehr
130/-/Panzer-Lehr
III/130/Panzer-Lehr
PzJg-Lehr/-/Panzer-Lehr
331/-/Panzer-Lehr
I/986/276th Infantry
II/986/276th Infantry
III/986/276th Infantry
I/987/276th Infantry
II/987/276th Infantry
III/987/276th Infantry
I/988/276th Infantry
II/988/276th Infantry
III/988/276th Infantry
I/276/276th Infantry
II/276/276th Infantry
III/276/276th Infantry
IV/276/276th Infantry
276/-/276th Infantry
101st SS Pz. Abt.
503rd Pz. Abt.
I/100/21st Pz.
II/100/21st Pz.
I/125/21st Pz.
II/125/21st Pz.
220/-/21st Pz.
I/155/21st Pz.
II/155/21st Pz.
III/155/21st Pz.
305/-/21st Pz.
I/12-SS/Hitlerjugend
I/25-SS/Hitlerjugend
II/25-SS/Hitlerjugend
III/25-SS/Hitlerjugend
I/26-SS/Hitlerjugend
II/26-SS/Hitlerjugend
III/26-SS/Hitlerjugend
II/12-SS/Hitlerjugend
III/12-SS/Hitlerjugend
IV/12-SS/Hitlerjugend
12/-/Hitlerjugend
RRC/4/2nd Infantry
RHL/4/2nd Infantry
ESR/4/2nd Infantry
BWC/5/2nd Infantry
LRM/5/2nd Infantry
CH/5/2nd Infantry
LFM/6/2nd Infantry
QCH/6/2nd Infantry
SSR/6/2nd Infantry
8/-/2nd Infantry
4/-/2nd Infantry
5/-/2nd Infantry
6/-/2nd Infantry
2/-/2nd Infantry
3/-/2nd Infantry
RWR/7/3rd Infantry
RRR/7/3rd Infantry
CSR/7/3rd Infantry
QOR/8/3rd Infantry
LDC/8/3rd Infantry
NSR/8/3rd Infantry
HLI/9/3rd Infantry
SDG/9/3rd Infantry
NNH/9/3rd Infantry
7/-/3rd Infantry
CHO/-/3rd Infantry
12/-/3rd Infantry
13/-/3rd Infantry
14/-/3rd Infantry
3/-/3rd Infantry
4/-/3rd Infantry
1SR/8/3rd Infantry
2EY/8/3rd Infantry
1SL/8/3rd Infantry
2LR/9/3rd Infantry
1KSB/9/3rd Infantry
2RUR/9/3rd Infantry
2RW/185/3rd Infantry
1RN/185/3rd Infantry
2KSL/185/3rd Infantry
33/-/3rd Infantry
76/-/3rd Infantry
8/3/6th Para
9/3/6th Para
1C/3/6th Para
7/5/6th Para
12/5/6th Para
13/5/6th Para
8RS/44/15th Infantry
6KOSB/44/15th Infantry
7KOSB/44/15th Infantry
6RSF/45/15th Infantry
9C/45/15th Infantry
10C/45/15th Infantry
10HLI/46/15th Infantry
11HLI/46/15th Infantry
2GH/46/15th Infantry
129/-/15th Infantry
130/-/15th Infantry
131/-/15th Infantry
64/-/15th Infantry
1H/128/43rd Infantry
2H/128/43rd Infantry
5H/128/43rd Infantry
4SLI/129/43rd Infantry
4W/129/43rd Infantry
5W/129/43rd Infantry
7H/130/43rd Infantry
4D/130/43rd Infantry
5D/130/43rd Infantry
94/-/43rd Infantry
112/-/43rd Infantry
141/-/43rd Infantry
59/-/43rd Infantry
RAF
1/27/7th Armoured
2/27/7th Armoured
1/22/3rd Armoured
1/2/3rd Armoured
2/2/3rd Armoured
8KRH/-/7th Armoured
4S/152/51st Infantry
6S/152/51st Infantry
4C/152/51st Infantry
4BW/153/51st Infantry
5G/153/51st Infantry
6G/153/51st Infantry
6BW/154/51st Infantry
7ASH/154/51st Infantry
8ASH/154/51st Infantry
75/-/51st Infantry
76/-/51st Infantry
77/-/51st Infantry
51/-/51st Infantry
4L/146/49th Infantry
1KOYL/146/49th Infantry
H/146/49th Infantry
1WY/147/49th Infantry
1/6 DWR/147/49th Infantry
1/6L/148/49th Infantry
1/7 DWR/147/49th Infantry
1F/148/49th Infantry
8F/148/49th Infantry
69/-/49th Infantry
70/-/49th Infantry
71/-/49th Infantry
58/-/49th Infantry
5EY/69/50th Infantry
6GH/69/50th Infantry
7GH/69/50th Infantry
6DL/151/50th Infantry
8DL/151/50th Infantry
9DL/151/50th Infantry
2D/231/50th Infantry
1H/231/50th Infantry
1D/231/50th Infantry
50th Infantry
I/9-SS/Hohenstaufen
I+II/130/Panzer-Lehr
Luftwaffe
2nd Army
II/32/16th Luftwaffe-Feld
I/46/16th Luftwaffe-Feld
II/46/16th Luftwaffe-Feld
16th Luftwaffe-Feld
I/SS-1/LSSAH
II/SS-1/LSSAH
III/SS-1/LSSAH
I/SS-2/LSSAH
II/SS-2/LSSAH
III/SS-2/LSSAH
I/SS-Pz-1/LSSAH
II/SS-Pz-1/LSSAH
SS-Artillery-1/LSSAH
1-SS-Afk./-/LSSAH
1-SS-Flk./-/LSSAH
1-SS-Pzj./-/LSSAH
Hitlerjugend Remnants
B2 Airstrip (Bazenville)
[Panzergruppe West]
JG26
