MARETH LINE
March 19th, 1943

Your unit has been transferred to North Africa to help with the Tunisian campaign.  You have been assigned to Montgomery's 8th Army and will assist in breaking through the Mareth Line - a series of fortifications built by the French before the outbreak of war.  It was believed that the mountains to the southwest were impassable to mechanised transport, however some recon reports from the LRDG (Long Range Desert Patrol Group) suggest otherwise.  Your men are to move through these mountains and flank the Mareth Line, cutting off its defenders.
