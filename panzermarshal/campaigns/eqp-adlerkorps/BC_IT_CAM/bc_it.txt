British World Campaign (1940-1945)

For King and Empire

This is a British campaign covering the period of the war from 1940-45.  Beginning in East Africa you will face off against Italy and her colonial allies, before moving onto North Africa to face the Afrika Korps.  Following the successful operations in Africa the campaign then moves into the European theatre, starting with the struggle up through Italy, before shifting to the invasion and liberation of France.  In the final stages you will move through Belgium and Holland until you finally reach the Rhine and cross into Nazi Germany.

Campaign Designer: Technique

Playtesters: 1Terminator, PzManiac, Lobo, RedDrake, Devil's Brigade


Panzer Marshal Port: Nicu Pavel

