Mareth Line
Ghott El Fedjadj
Oudref
Gabes : PORT
Djebel Tebaga : ROUGH
El Hamma
Gabes
Jarba : PORT
Aghir
Adjim
El Kantara
Mareth
Wadi Zigzaou : RIVER
Matmata
Houjarie
Wadi Zeuss : RIVER
Medenine
Sabkhat al Milh : SWAMP
B.Soltane
Hallouf
Ben Gardane
Ksan Rhilane
Foum Tatahouime
Kirchaou
Oouirat
I/65/Trieste
II/65/Trieste
I/66/Trieste
II/66/Trieste
I/21/Trieste
II/21/Trieste
III/21/Trieste
Mareth Line
I/8. Bersaglieri/Giovani Fascisti
II/8. Bersaglieri/Giovani Fascisti
I/136/Giovani Fascisti
II/136/Giovani Fascisti
III/136/Giovani Fascisti
I/155/90th Leichte-Afrika
II/155/90th Leichte-Afrika
I/200/90th Leichte-Afrika
II/200/90th Leichte-Afrika
I/361/90th Leichte-Afrika
-/190/90th Leichte-Afrika
II/361/90th Leichte-Afrika
I/220/164th Leichte-Afrika
II/220/164th Leichte-Afrika
I/382/164th Leichte-Afrika
II/382/164th Leichte-Afrika
I/433/164th Leichte-Afrika
II/433/164th Leichte-Afrika
I/115/15th Panzer
II/115/15th Panzer
33/-/15th Panzer
I/8/15th Panzer
II/8/15th Panzer
8th Army HQ
2/SH/152/51st Infantry
5/SH/152/51st Infantry
5/QOCH/152/51st Infantry
126/-/51st Infantry
Saharian Grouping
'L' Force
I/350/Saharian Grouping
II/350/Saharian Grouping
I/290/Saharian Grouping
II/290/Saharian Grouping
[Armeegruppe Afrika]
5/BW/153/51st Infantry
1/GH/153/51st Infantry
5/7/GH/153/51st Infantry
127/-/51st Infantry
I./JG-77/II. Fliegerkorps
II./JG-77/II. Fliegerkorps
III./KG-77/II. Fliegerkorps
RAF
I/190/90th Leichte-Afrika
II/190/90th Leichte-Afrika
135th Flak Rgt.
-/190/90th Leichte-Afrika
[XX Corpo]
