TACTICAL VICTORY

Well done, commander.  Despite the odds being stacked against you, you managed to take the fort at Gallabat and the city of Metemma.  Unfortunately, we can not continue to hold these positions.  The Italians are much too strong at this point, and we need to save our strength for elsewhere.  The raid served its purpose however, and we can be thankful for that.