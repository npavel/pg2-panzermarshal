TACTICAL VICTORY

TO: Commanding General.
SUBJECT: Regrouping and movement.
  1. All the passes of the Eastern Dorsal are now under Allied control, you should disengage from the enemy and move reserve units in to mop up.
  2. We are regrouping our forces for the final assault on TUNIS, to this end you should move north as per the attached schedule...