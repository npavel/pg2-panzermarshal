TACTICAL VICTORY

TO: Commanding General.
SUBJECT: Victory at BONE.
  1. You are ordered to move your units to join the offensive in southern Tunisia. Transport has already been allocated and is assembling in the BONE area.
  2. As replacement British units move forward your units should be pulled off the line.
  3. On a personal note, you and your men should be commended for the masterful defense and countereattack at BONE.