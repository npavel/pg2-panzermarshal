LOSS

"The Germans are weak, Tunis will be captured and the invasion of Sicily will proceed as scheduled, but the British have already been criticizing us and this disaster is simply embarrassing. The General should be relieved of his command - a transfer to a colder climate is definitely in order."