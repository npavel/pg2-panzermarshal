17. MATEUR, 04/23/1943

TO: Commanding General.
SUBJECT: Operation VULCAN.
  1. Your Corps has been assigned the northern sector of Operation VULCAN with its main objective being BIZERTE, British units have been assigned to the assault on TUNIS.
  2. The initial assault should capture the mountainous area centering around HILL 609, between SIDI NSIR and MATEUR, and the GREEN and BALD HILL areas near DJEFNA.
  3. Your main goal for the first part of the offensive is to capture MATEUR and then push on to BIZERTE.
  4. Despite some negative comments from our British allies I expect you to provide them with the fullest co-operation...