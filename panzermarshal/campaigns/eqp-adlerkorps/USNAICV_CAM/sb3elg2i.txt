15. EL GUETTAR 2, 03/28/1943

TO: Commanding General.
SUBJECT: Counterattack at EL GUETTAR.
  1. Your are ordered to attack from the area around EL GUETTAR, break the Axis lines and head for the coast.
  2. The major objective is to keep pressure on the Axis forces, who are also engaged with the British 8th Army at WADI AKARIT.
  3. A bold stroke could force the withdrawal of the Axis from all positions in the south...