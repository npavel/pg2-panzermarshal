BRILLIANT VICTORY

TO: Commanding General.
SUBJECT: Defense of SALERNO beachhead.
  1. You and your men have saved a bad situation.
  2. We intend to start the push north as soon as the local situation is stabilized, report the status of your units as soon as possible...
