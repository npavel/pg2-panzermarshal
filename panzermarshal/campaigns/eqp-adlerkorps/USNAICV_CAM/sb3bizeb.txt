BRILLIANT VICTORY

TO: Commanding General.
SUBJECT: Victory in TUNISIA.
  1. All resistance has ended in the north and the British and French armies are mopping up pockets of resistance near TUNIS.
  2. You are ordered to report to HQ to discuss plans for the invasion of Italy.