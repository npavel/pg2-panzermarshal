19. GELA, 07/10/1943

TO: Commanding General.
SUBJECT: Operation HUSKY.
  1. The objective of Operation HUSKY is to establish beachheads in south eastern SICILY. The beachheads will be followed up with the capture of SICILY. Control of SICILY is a vital stepping-stone to an invasion of mainland ITALY and will facilitiate control of the Mediterranean.
  2. The US Army has been assigned the landings at GELA and, as advised, you will be in command. I cannot repeat my statements at our recent briefing too often - I have elevated you to command of the GELA landings over the objections of my superiors, so do not disappoint me.
  3. Your initial objectives are as previously advised; land quickly, expand and hold the bridgehead.
  4. The quality of the defense is unknown at this stage, so you should prepare for the worst. Our best Intelligence suggest the Italian army are manning the initial defenses and are close to collapse. Relevant Intelligence will be transmitted as we receive it...