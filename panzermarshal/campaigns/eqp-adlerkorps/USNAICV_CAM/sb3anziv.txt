VICTORY

TO: Commanding General.
SUBJECT: ANZIO beachhead.
  1. Even though opposition was light, your gains have been impressive.
  2. I understand your eagerness to advance further, however I disagree as to the extent of the German defences. Your orders are to dig in and await further orders. I have no intention to order an advance until the build-up has been completed.

(The war continues, but the US North Africa & Italy campaign ends here. Or does it?)