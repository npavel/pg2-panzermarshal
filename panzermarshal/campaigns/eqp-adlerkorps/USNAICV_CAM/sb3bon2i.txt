09. BONE 2, 03/01/1943

TO: Commanding General.
SUBJECT: Counterattack ordered at BONE.
  1. You should now be reinforced and ready to counterattack after our setbacks around and east of KASSERINE PASS.
  2. You are to advance south from our positions in the area of BONE and attack the German and Italian column moving up from KASSERINE. The British 8th Army is approaching MEDENINE; you must succeed here and push the enemy back or the North African theater is in jeopardy.
