US NORTH AFRICA & ITALY CAMPAIGN
US NORTH AFRICA & ITALY CAMPAIGN

Version: 4.1
Equipment: Adlerkorps 2000/2000 (version 0.68 beta or later) (any betaversion after 0.615 should work)
Number of Scenarios/Longest Path: 24/19
Player Country: USA
Campaign Designer: Steve Brown
Campaign Converter: Jan Hedstrom (aka PzManiac)

Maps needed...

Anzio            |     Kasserine
Bona             |     Maknassy
Casablanca   |     North Sicily
El Guettar    |     Oran
Europe         |     Port Lyautey
Fondouk        |     Tunis
Gela             | 

The Campaign...

This campaign starts with Operation Torch in November 1942 (your choice of commanding the Casablanca, Oran or Port Lyautey landings), continues to Tunisia, Sicily and finishes at Salerno and Anzio 1943-1944. It also has some player-determined scenario choices, in 2 places YOU decide which scenarios you want to play. There are a couple of non-historic scenarios in North Africa that you play after a loss in some of the historic ones.

Please read the readme first for installation instructions, particularly if you are a new player. 

Conversion History...
This conversion was originally made using the version 2.0. The first version 4.0 was released in 12/2002.
This is a revised (and more difficult) version.

Credits...
A special thanks to Steve for letting me convert this excellent campaign. Thanks also to the guys that played this campaign
as a campaign challenge at JP's Panzer in June-July 2003. Special commendation to Darek, Dennis Felling and MJ Patterson for their particular
comments regarding improvements in the campaign.

Jan Hedstrom, 07/2003

Any feedback on this conversion can be addressed to either myself or Steve

jhe@ps.no

or

steve@magnecor.com
pg2campaigns@wargamer.com

http://go.to/panzergeneral2
http://www.wargamer.com/pg2campaigns