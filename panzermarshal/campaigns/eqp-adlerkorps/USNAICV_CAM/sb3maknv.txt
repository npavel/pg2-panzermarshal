VICTORY

TO: Commanding General.
SUBJECT: Capture of MAKNASSY PASS.
  1. Control of the lower part of the eastern dorsal is secure, reserve units should be moved forward. You will move north to join an assault on the FONDOUK GAP...
