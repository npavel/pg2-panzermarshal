24. SALERNO 3, 09/20/1943

TO: Commanding General.
SUBJECT: Breakout from SALERNO.
  1. It appears that most of the German defenders have withdrawn north towards NAPLES. Now is the time to breakout from the beachhead.
  2. The main effort will be north, towards NAPLES via the coast roads and north of MOUNT VESUVIUS. In addition control of the SORRENTO peninsula should be completed and a smaller force should clear the mountains of German forces...