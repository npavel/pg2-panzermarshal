26. OPERATION SHINGLE, 01/22/1944

"...As you know, over the last couple of months we have advanced north past Naples but are now stalemated along the so-called Gustav Line. Our advance, particularly in our own sector around Cassino and along the Gargliano has ground to a halt.

"A bold stroke is needed to break the stalemate and we have decided to land here, at Anzio - about 30 miles south of Rome. Our hope is to so disrupt the German defenses that we can rapidly advance the bulk of the army to Rome. The first part of the plan, codenamed Operation Shingle, will be to land a small force here and here and hold the beachhead. The plan must be executed rapidly because many of our landing craft are scheduled to be transferred to England for operations later in the year.

"As you are no doubt well aware, there has been some question about the objectives of the operation, but all you need to know is the operation will go ahead as planned..."
