WESTERN TASK-FORCE CHOICE

Which Operation Torch landing do you want to command? Casablanca or Port Lyautey?

PLEASE NOTE: Go directly to your choice, otherwise you may end up commanding an unexpected battle!
