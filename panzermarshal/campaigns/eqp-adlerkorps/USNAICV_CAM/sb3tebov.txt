VICTORY

TO: Commanding General.
SUBJECT: Victory at TEBOURBA.
  1. The supply route from MEDJEZ EL BAB to TEBOURBA is open, although the assault on Tunis has stalled.
  2. Prepare your defenses for an Axis counterattack ...