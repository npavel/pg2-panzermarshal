03. MEDJEZ EL BAB 2, 12/22/1942

TO: Commanding General.
SUBJECT: Reinforcement of British positions.
  1. As you have been informed in previous communiations the Christmas offensive to capture TUNIS is faltering.
  2. You are to advance east of MEDJEZ EL BAB and relieve British units, who are heavily engaged in the mountains. The positions must be held at all costs or we risk a major defeat ...