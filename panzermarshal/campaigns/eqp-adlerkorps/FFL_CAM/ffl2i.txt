Bir-Hakeim

26 May 1942

When Rommel launched the Gazala offensive, the XX Italian Corps was supposed to secure the cauldron of Bir-Hakeim, south of the axis position. The Italians lost this bloody battle, forcing Rommel to intervene and thereby causing the Gazala offensive to stall.  The Free French eventually evacuated but because of their valiant stand the Allies weren't defeated at Gazala.