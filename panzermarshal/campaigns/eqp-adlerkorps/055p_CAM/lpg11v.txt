The South African 2d Division of 30 Corps, British Eighth Army, have successuflly stormed the Afrika Korps redoubt of Bardia-Sollum. Deprived of their chief source of water and under pressure from your troops, the Halfaya garrison have had no choice but to finally give up their resistance.  

With the destruction of the Axis forces in East Cyrenaica and reopening of the lines of  communication  from El Agheila into Egypt, the first phase of the Libyan campaign is successfully concluded. It is time to press on towards Tripoli and complete victory in North Africa.



