Greco-Italian War - Klisura Pass - 1941

On 14 Nov 1940, the Greeks counter-attacked and pushed the Italians back some 80 Km into Albania, II Corp taking Plemeti by Dec 4 and I Corps taking  Argirocastro  by  Dec. 9. The Italians have set up a defence sector hinged on Tepeleni and Klisura.  Tepeleni blocks the road to the vital port of Valona, Klisura the road to Berat, northeast of Valona.  II Corps is to resume their advance on 4 Jan and attack Italian positions at Klisura, comprised of four Italian divisions, no later than 8 Jan 1941.

