Rommel consolidated his forces, resupplied, and burst forth from El Agheila on 21 Jan 1942, quickly retaking Benghazi and driving on toward Tobruk.  

Despite capturing huge stores of British supplies along the way, Rommel has once again stretched his supply lines to their limit.  During the battle, German supply routes were being hindered by the SAS and Long Range Desert Group and both these forces did great damage, playing a major part in halting Rommel's advance.


Your actions in the area of Carmusa succeeded in slowing the German attack, allowing British forces to fall back on a series of defensive fortifications being prepared called the Gazala Line. Here we will be able to pause to regroup and strengthen our forces. Though Rommel was able to reach the Gazala line with a large force, his attack has faultered due to his running out of supplies, especially fuel.  I think we will be able to hold him here a while.