Operation 'Ironclad' was the first large scale amphibious assault made by British forces since the attempt to storm the Dardanelles in the First World War; and was extreemly successful, though further landing craft developement is required for future amphibious operations.

However, due the obstinacy of the French Commander-in-Chief, who refuses to surrender, very bad weather and difficult terrain Vichy French troops in the area have been able to quickly withdrawn to the south and continue the campaign, though at a low level of intensity. Involvement of Japanese forces in the battle only confirm our intitial fears.  

Unfortunately, due to this 'heroic' defence there is a now a concern that the island's meager forces may be reinforced by an Italian or Japanese relief force. Remember, Admiral Nagumo's Fast Carrier Strike Force is still in the area after it's attack on Ceylon.

