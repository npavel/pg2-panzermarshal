Well done, Commander.  A quick, sizable German incursion in support of Rashid Ali would have likely succeeded in providing Nazi Germany a foothold in the Middle East.  The implications of the Habbaniya battle are staggering and I don't know what Adolf is thinking to have missed this chance.

I have a message from PM Churchill: "Your vigorous and splendid action has largely restored the situation. We are watching the grand fight you are making." 

Now to Baghdad.