Although the we were only able to throw a relatively small force at the problem, the ill-equipped and trained Iraqi forces were no match.Supported only by a few squadrons of German and Italian aircraft, Iraqi forces have been quickly quickly scattered by our forces.

However, the taking of Baghdad proper took much longer as time and resources were expended in house-to-house street battles.  By the time Baghdad was secure, Rashid Ali and his gang had escaped to Persia.

An armistice has been signed and the Regent reinstated.  The terms of the armistice require that all axis personnel in Iraq are to be interned and that Iraq support the British cause against the axis.  The Iraqi army, essentially 2nd Div in Kirkuk and Mosal, is to be disbanded and reformed.