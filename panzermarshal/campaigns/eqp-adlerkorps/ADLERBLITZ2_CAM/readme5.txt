Campaign Introduction by Jan Hedstr�m for version 5.1
This is a conversion of Craig Miller's excellent Greater Europe-campaign to the Adlerkorps.
The conversion is made from the Waffenkammer e-file using Lasse Jensen's scenario editor. 

1. Special requirements for this version
For those unfamiliar with playing user-made campaigns, please read Whoopy-Cat's excellent
PGII for Dummies first. This document is to be found at the Boot Camp at JP�s Panzers: http://pub131.ezboard.com/fjpspanzersfrm4.showMessageRange?topicID=254.topic&start=1&stop=20. 
This document will save you from a lot of aggravations and troubles that might arise when playing a user-made campaign for the first time

1.1 E-file requirement
This version is made for Mikael Adler's 2000/2000 e-file. The latest official version at the time of the conversion was beta 0.72. The latest version of Adler's e-file is to be found at;
http://www.medio.mh.se/~vonadler/pg2/
Please note that this version is NOT compatible with Adler's 1000/1000 e-file, since it uses units that aren't included in that particular version.

1.2 PGII version requirement
Please observe that this e-file (like all e-files that exceed 1000 units) requires you to play the campaign with the unofficial PGII 1.02G version, which is to be found at http://www.strategyplanet.com/panzergeneral/PG2Main_news.html (at the version and patches section). Please note that this
version is NOT compatible with the standard PGII 1.02 version, and the game is likely to crash should you try playing with the standard PGII 1.02 version.

1.3 Icon upgrades (dat-files)
his version also requires the icon upgrades which are relevant to the particular version of the Adler file your using.
For betaversion 0.72 you will need datup part 1 released in early August 2003. This datup is to be found at 
the equipment section of http://www.strategyplanet.com/panzergeneral/PG2Main_news.html

Please note that without a correct icon upgrade some icons will appear strangely and other icons will 
be used instead of the proper ones, so you really can�t do without them.

1.4 User-made maps (.map-files AND .shp-files)
Most of the troubles in playing a user-made campaign seem to arise from missing user-made maps. 
Please read the Dummies document written by Whoopy-Cat first and MAKE SURE you install BOTH the .map file
and the .shp files in the correct folders. The shp files can be downloaded from several sources (at the map section of Lasse�s site http://www.strategyplanet.com/panzergeneral/pg2main_equipment.html
or at Eric�s site http://panzergeneral.net/

The necessary map files are already included in this download. 

In addition to standard SSI maps the following 19 maps are required.

Agedabia, Alexandria,Algiers, Arnhem2, Bardia, Don, El Alamein/Alamein,
England, Gibraltar Plus/Gib2, Kazan,Konrad2, Kreta2,LeningradEast/Goworow, Litovsk,
Moscow, Narvik, Sevastopol,Stalingrad/Stalingd, Tunis

Some comments on confusion around map names: The El Alamein map is called Alamein, The Gibraltar Plus map is
also called Gib2. When it comes to the Goworow map, this map file uses the same .shp-files as
the Leningrad East map so you need to download the shp-files for the LeneEast map. Please note
that there are two Sevastopol and two Stalingrad map files (but only one set of .shp-files each).
This campaign uses the Sevastopol.map and the Stalingd.map (both are included in the download)


2. Naming of the files
All files in this version except this file have their original names, please don't mix files from this version with files from the Waffenkammerversion.

3. Further information about the conversion
3.1 Changes in scenarios
Due to the inclusion of several new scenarios in the beginning of the campaign (compared to the original SSI Blitzkrieg), several of the standard SSI scenarios are much too easy, since your army is much more experienced. I have NOT changed any of these scenarios since this would affect the original SSI scenarios included in this campaign. An installation of this campaign will 
therefore not change any of theses original scenarios. There are very few changes done to the non-SSI scenarios. I have mostly focused on substituing strange units into the units that were supposed to be included in the scenario. But I've also changed some of the infantry units assigned to the AI to other infantry units (always with better stats), in order to profit from the multitude
of units available in the Adlerkorps. As an example in late-war Soviet scenarios regular infantry and guards unit have been changed into 44 Pehota and 44 Gvardevskaya (instead of using the original Pehota and Gvardevskaya units). 

I should also point out that NO changes have been made to the scenarios that lie outside the longest path; the two Windsors ('40 and '43) (no 9 and 18 in the scenario list) and the Defending the Reich scenarios (winter Storm to Konrad, no 21 to 26 in the scenario list). These are all original SSI
scenarios and would be affected permanently by any change of unit numbers. Players choosing to play any of these scenarios might therefore come across strange units due to possible incompatibility between standard SSI unit numbers and the Adlerkorps unit numbers. However, no such incompatibilites
where found in the standard SSI scenarios included in the longest campaign path, so without having playtested these particular scenarios, I would guess that you could play them without encountering too many strange units.

3.2 Removal of the Casablancascenario
In the original Waffenkammerversion, there is included a Casablanca-scenario. This scenario is however NOT used in the campaign and I have NOT seen the point in including those files in this version. All references to this scenario have been removed from the original read-me text as well.

3.3 Removal of the zmdrd200 and zomdrd400
In the Waffenkammerversion Craig Miller had included to save-me files which would allow the player to play against an AI which had more prestige available. Since I haven't playtested these files myself I've decided against including them in this version.

3.3 Bug in the Waffenkammer version of this campaign
There are two Japanese units that appear in the Waffenkammerversion (a Japanese tank at Windsor instead of a railroad gun, and a Japanese fighter at Seelow instead of an AD unit?). In the Adlerkorpsversion a railroad gun and an AD unit will appear instead.

4. Files in this campcm5.zip
This file campcm5.zip contains the following files:
campcm4.cam   An extended campaign (Greater Europe - Version 5.0) for Panzer General 2
campcm4.txt   A file associated with campcm4.cam
blitz404.txt  A file which lists the campaign scenarios and indicates the paths after various results for the different scenarios.
readme5.txt  This file
Seventeen map-files
A group of scenario files:  cm3*.scn, cm3*.txt, cm4*.scn, cm4*.txt
A group of text files associated with specific scenarios: cge*.txt
These last groups of files begin with cm3, cm4,  or cge to make it easier to remove them later. 

To use, copy all these files into the Scenario Subfolder of your Panzer General 2 Folder. 

5. Sounds in this campaign
I personally haven't use any sound and video but here are Craig Miller's own words about these files. "This campaign also includes many of the great fas*.smk video/audio files created by Andreas Seidel. They are available at his web site www.waffenkammer.com   I used his 2,3,5,7-9,11,13-16 fas files. I've also made up many new .mus files to go with my custom txt files for initial briefings and
brilliant/victory statements.  These files are available as the zipped files hcm4a,b,c,d mus.zip. If you don't have either the new smk or mus files, the campaign will still run without them"

6. Important note regarding campaign path
This conversion presupposes that you play the longest path, meaning that you'll have to take a Victory at Dunkirk and at Klin, meaning that you'll have to avoid Brilliant Victories in those two scenario otherwise you'll end the campaign prematurely.

7. Craig's own words and acknowledgements about the creation of this campaign
I created this extended campaign because I was dissatisfied with the endings of the standard Panzer General 2 German Blitzkrieg campaign.  After Klin a Brilliant Victory or a Victory led to a 1943 invasion of England if England were still in the war, or directly to an invasion of the US if England had been defeated.  A Tactical Victory led to six scenarios fought under increasingly
adverse overall conditions. I decided that a successful invasion of England in 1941 (Windsor)  followed by a Brilliant Victory at Klin would probably lead to a German victory in Europe before December 1941, so the US and Germany would not have been at war. Thus I omitted the SSI invasion of the US.  A Brilliant Victory at Klin with England still in the war leads to an invasion of 
England in 1943 as before, but now I assumed that a victory here in 1943 would again lead to a German victory in Europe, mainly because a German invasion of the US seemed too implausible.  I assumed that a Victory at Klin would not cause the Soviet Union to surrender.  

I decided to try to develop a series of scenarios which would come into play after a Victory at Klin. I assumed that after such a Victory the arrival of fresh Soviet troops from Siberia would force the Germans to evacuate Moscow.  My campaign then continues with a German drive into Southern Russia in Spring 1942.  The initial SSI campaign leading up to Klin I thought very well done,
so I made no changes in the original SSI scenarios, although I did add scenarios between some of the SSI scenarios.  The main changes I made in the original SSI part of the campaign were:
(1) Dunkirk- A Victory now leads to a new scenario, Drive on Paris
(2) Klin-    A Brilliant Victory leads to Windsor43 (as SSI) if England were still in the war; 
but if England has surrendered a Brilliant Victory at Klin results in a German Victory. 
A Victory at Klin leads to a new Thermopylae42 scenario. 
(3) Windsor43- Any victory leads to a German Victory.

This campaign contains 52 scenarios.  The longest sequence possible is 42 scenarios (12 original SSI and 30 custom scenarios). My additional scenarios are meant to be plausible, if not historic.  Particularly the campaign
switches between fronts more rapidly than might have been realistic.

The scenarios were developed using the stock SSI scenario editor in conjunction with the excellent scenario editor developed by Lasse Jensen.The campaign (and the conversion) could not have been done without his superb Campaign Editor.

My scenarios were designed to be used in a campaign game.  At Lasse Jensen's suggestion most of my custom scenarios are marked non-active, which means that they will not show up as individual scenarios but will appear when needed in a campaign.  I enjoyed developing this campaign and these scenarios and hope it gives some pleasure to others.  If you find any errors or have suggestions for improvements please send them to me. 

Acknowledgments: 
The initial Brest-Litovsk briefings and victory statements are based upon those created by Andreas Seidel as part of his Brest-Litowsk scenario. 
The scenario Invasion England 1945 is based on a Scenario Invasion England by Lasse Jensen.
I modified it for use in a campaign in the spring of 1945. The scenarios Sevastopol and Stalingrad were made by Lasse Jensen.  I modified them slightly for use in this campaign.   

8. Difficulty level
This is not a difficult campaign although it might be slightly more difficult due to the  difference in equipment costs between the Waffenkammer e-file and the Adlerkorps. I chose NOT to make this campaign any harder since I thought it would be fun to play a campaign with the Adlerkorps where you actually can afford to buy the most expensive equipment and at the same time field an air force. Experienced players or players wanting more of a challenge should definitely play this campaign at 75% or lower. 

The prestige settings at 100 will give you ample room to upgrade to the latest equipment along the way, provided you don't build a much too large army. Players used to be playing without an air force should note, that you won't get through this campaign without an air force (mainly due to the scenario, Invasion England, meaning that you'll need 4-6 fighters in your core)
 
I would also recommend you to limit the number of units in your army to around 40-50, depending on what prestige level you're playing, otherwise you will have problems upgrading all units to expensive late-war equipment.

9. Prestige
Prestige caps have been significantly lowered to better reflect the size of your army, and prestige awards have been significantly enhanced in the later part of the campaign to take into account the more expensive equipment in the Adlerfile. The prestige cap levels presupposes that you'll get a  prototype at approximately every third scenario on an average, meaning you'll have around 10 prototypes in addition to the six core units. As you probably know these units don't count against the cap levels, in consequence the cap levels therefore only take into account the 25-30 units you buy along the way, so if you want to stay under the cap (which is highly advisable if you want to get the prestige awards
inbetween the scenarios) you might want to restrict the number of expensive buys (like the 21K38 which costs around 2500)

10. Playtesting
The longest path of this campaign has been playtested twice both at 50% and at 100%. Like stated before the scenarios that IS NOT included in that particular campaing path, has NOT been playtested, mainly because I assume that most players playing this campaign plays the longest path.

11. Revision history and my own acknowledgements
For the 5.1 version I would like to take the opportunity to thank Darek Koncewicz (aka Darek22) for his comments on playing my original version of Greater Europe, version 5.0. 
Several small mistakes in that release has been changed thanks to his comments. In addition the caps have been reduced somewhat in comparison to the earlier version.
In addition there has been the following map changes: The Arnhem scenario now uses the Arnhem2 map, The Leningrad scenario now uses the Goworow map, and the Operation Konrad
scenario now uses the Konrad2 map. This is also the last revision I will make of this campaign. Players wanting to play a more competitive version of this campaign should instead try my modified version
of this campaign, which is to be found under the name of MillerBlitz! 
