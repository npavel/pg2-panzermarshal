With the Allies forced out of Northwest Afrika we can now concentrate on taking Egypt and the Suez Kanal.  Our immediate objective is to capture Bardia.  The English have had time to reinforce and dig in, so this will not be an easy assignment, but it is critical.  You must achieve your objectives quickly so we may maintain our pressure on the British.



 