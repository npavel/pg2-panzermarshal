Between August 1 and August 10, 1944 at the conclusion of the Belorussian strategic offensive operation near the town of Radzymin in the vicinity of Warsaw, part of which entailed a large tank battle at Wolomin. It was the largest tank battle on the territories of Poland during WWII.

Approach of the Red Army forces into the proximity of Warsaw served to initiate the Warsaw Uprising by the Home Army with expectation of help from the Red Army.

Further combat lasted until August 10, when the Germans finally withdrew. Soviet losses were heavy, but not heavy enough to affect the overall course of their thrust to the vicinity of Warsaw. The 3rd Tank Corps was destroyed, the 8th Guards Tank Corps took heavy losses, and the 16th Tank Corps took significant losses as well. 