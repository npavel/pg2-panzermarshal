Bobruisk was liberated on June 29, the  German 383rd Infantry Division commencing withdrawal towards dawn: no further elements of Ninth Army would escape from east of the Berezina. The German breakout had allowed around 12,000 troops - mostly demoralised and without weapons - from the pocket east of Bobruisk to get out, but the Soviets claimed 20,000 taken prisoner. A further 50,000 were dead: Soviet accounts speak of the area being carpeted with bodies and littered with abandoned materiel. The Soviet writer, Vasily Grossman, entered Bobruisk shortly after the end of the battle:

"Men are walking over German corpses. Corpses, hundreds and thousands of them, pave the road, lie in ditches, under the pines, in the green barley. In some places, vehicles have to drive over the corpses, so densely they lie upon the ground [...] A cauldron of death was boiling here, where the revenge was carried out."

Ninth Army had been decisively defeated, and the southern route to Minsk was open.

