Colmar Pocket:

Herr General due to your poor performante in Ardennes you have been assigned to the command
of the troops of City of Colmar. An allied attack is expected in almost anytime, the eight
division to your command are very covered but you won't have more than one unit of tanks.
Only artillery have enough ammunition.
Try to keep the terrain as much as you can.

Good Luck!