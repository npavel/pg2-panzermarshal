Suffering from the reverse in Egypt and the lack of supplies and reinforcements , 21 Panzer must fight it out to hold Banghazi. A defeat here would surely mean the end of the North Africa expedition.

Herr general, we have managed to give you some troops to defend the hastily prepared fortifications at Banghazi. You should be able to fight a delay action and stop them at this line.
Be careful of their naval units and RAF. 