Warsaw
River Wkra
Nasielsk
River Narew
Wyszkow
River Bug
Zatuski
Zabrodzie
Dabrowka
Modlin-Twierdza
River Vistula
Zakroczyn
Poswietne
Kampinoski Park Narodowy
Legionowo
Warsaw
Dluga Szlache
Kobylak
Grady
Cygan
Blonie
Pruszkow
Minsk Mazowiecki
Kaski
Otwock
River Wieprz
Grodzisk Mazowiecki
Warsaw Airport
River Pilicc
Piaseczno
Karczew
Katy
Zyrardow
Gora Kalwaria
Mszczonow
Paplin
Garwolin
Grojec
Wilga
7 Pz R/Pz Kempf/[I]
SS[D]/Pz Kempf/[I]
SS AR/Pz Kempf/[I]
502 Pion./Pz Kempf/[I]
Pi.R.St.V/[3 Armee]
9 MG Btn./[3 Armee]
Grz.Schtz.31/[KW]
1 R/1 ID/[KW]
22 R/1 ID/[KW]
43 R/1 ID/[KW]
1 AR/1 ID/[KW]
27 R/12 ID/[KW]
48 R/12 ID/[KW]
89 R/12 ID/[KW]
12 AR/12 ID/[KW]
1 Lpolizei R./[3 Armee]
41 Bru.Btn./[3 Armee]
2 R/11 ID/[I]
23 R/11 ID/[I]
43 R/11 ID/[I]
11 AR/11 ID/[I]
11 Pzjgr/11 ID/[I]
11 Pion./11 ID/[I]
151 R/12 ID/[I]
162 R/12 ID/[I]
176 R/12 ID/[I]
12 AR/12 ID/[I]
SS[LAH]/[XIII]
31 R/24 ID/[X]
32 R/24 ID/[X]
102 R/24 ID/[X]
24 AR/24 ID/[X]
24 Pion/24 ID/[X]
41 R/10 ID/[XIII]
20 R/10 ID/[XIII]
85 R/10 ID/[XIII]
1 AR/10 ID/[XIII]
[Luftwaffe]
58 AR (Mot.)/-/[X]
21 R/17 ID/[XIII]
55 R/17 ID/[XIII]
95 R/17 ID/[XIII]
17 AR/17 ID/[XIII]
88 Pion/10 ID/[XIII]
73 R/19 ID/[XI]
74 R/19 ID/[XI]
59 R/19 ID/[XI]
19 AR/19 ID/[XI]
39 R/24 ID/[XI]
77 R/24 ID/[XI]
78 R/24 ID/[XI]
26 AR/24 ID/[XI]
30 R/18 ID/[XI]
51 R/18 ID/[XI]
54 R/18 ID/[XI]
74 AR/18 ID/[XI]
1 Pz Bde./1 Pz Div./[XVI]
1 Pz Bde./1 Pz Div./[XVI]
1 Schzt./1 Pz Div./[XVI]
2 Mot./1 Pz Div./[XVI]
73 AR/1 Pz Div./[XVI]
5 Pz Bde./4 Pz Div./[XVI]
5 Pz Bde./4 Pz Div./[XVI]
103 AR/4 Pz Div./[XVI]
3/79 Pion./4 Pz Div./[XVI]
12 Schzt./4 Pz Div./[XVI]
1 Lt.Tk./[Warsaw]
2 Lt.Tk./[Warsaw]
73 Tk.Co.(Ind.)
71 Tk.Co.(Ind.)
41 Tk.Co.[Crew]
15 ID/[Pomorze]
5/Pod.Bde./[OG Narew]
10/Pod.Bde./[OG Narew]
9/Pod.Bde./[OG Narew]
14/Pod.Bde./[OG Narew]
53/Pod.Bde./[OG Narew]
33 R/18 ID/[OG Narew]
42 R/18 ID/[OG Narew]
71 R/18 ID/[OG Narew]
38 MG C./18 ID/[OG Narew]
18 KS/18 ID/[OG Narew]
18 H/18 ID/[OG Narew]
18 Lt./18 ID/[OG Narew]
18 E/18 ID/[OG Narew]
13 R/8 ID/[Modlin]
21 R/8 ID/[Modlin]
32 R/8 ID/[Modlin]
8 KS/8 ID/[Modlin]
11 MG/8 ID/[Modlin]
8 AA/8 ID/[Modlin]
8 Btn./8 ID/[Modlin]
8 AR/8 ID/[Modlin]
78 R/20 ID/[Modlin]
79 R/20 ID/[Modlin]
80 R/20 ID/[Modlin]
92/20 ID/[Modlin]
81 AA/20 ID/[Modlin]
20 E/20 ID/[Modlin]
62 Ind./20 ID/[Modlin]
20 Lt./20 ID/[Modlin]
20 H./20 ID/[Modlin]
88 Btn./-/[Modlin]
98 Btn./-/[Modlin]
II/[Warsaw]
I/[Warsaw]
1 MG/-/[Modlin]
71 Lt./-/[Modlin]
1 AR/-/[Modlin]
60 E/-/[Modlin]
133 R/RID/[OG Narew]
134 R/RID/[OG Narew]
135 R/RID/[OG Narew]
32 Lt./RID/[OG Narew]
43 E/RID/[OG Narew]
[L4]
[L4]
15 ID/[Pomorze]
15 ID/[Pomorze]
15 ID/[Pomorze]
15 ID/[Pomorze]
25 ID/[Pomorze]
25 ID/[Pomorze]
25 ID/[Pomorze]
