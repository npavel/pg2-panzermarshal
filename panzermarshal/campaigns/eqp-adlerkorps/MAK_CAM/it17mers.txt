Mersa Matruh
Mare Nostrum
Coastal Road
Trail
Mersa Matruh
Charing Cross
Sidi Hamza
Minqar Qaim
Bir Khalda
Bir Sarahna
Garawla
Smuggler's Cove
Bir El Hukuma
Northern Escarpment
Southern Escarpment
Bir Abu Shayit
Bir Abu Batta
Escarpment (Passable)
Mahatt Abu Batta
Qabr Gadalla
Masrab Khalda
Masrab El Istabl
4/6Rajputana/5/10Ind [X]
3/10Baluch/5/10Ind [X]
1/Essex/5/10Ind [X]
4/13FF/21/10Ind [X]
2/8Gurkha/21/10Ind [X]
1/KORR/25/10Ind [X]
1DCLI/21/10Ind [X]
3/5Mahratta/25/10Ind [X]
2/11Sikh/25/10Ind [X]
HQ 10 Indian Division [X]
30Lt/-/10Ind [X]
68FdRA/-/10Ind [X]
97Fd/-/10Ind [X]
97FdRA/-/10Ind [X]
164FdRA/-/10Ind [X]
155Lt/-/-/10Ind [X]
A/1/RNF/-/10Ind [X]
B/1/RNF/-/10Ind [X]
C/1/RNF/-/10Ind [X]
6/DLI/151/50 [X]
8/DLI/151/50 [X]
9/DLI/151/50 [X]
74FdRA/151/50 [X]
1/25Lt/151/10 [X]
69/50 [X]
92Sqn
1/5Mahratta/29/5Ind [XIII]
3/2Punjab/29/5Ind [XIII]
2/Highland/29/5Ind [XIII]
32FdRA/29/5Ind [XIII]
1FdRA/29/5Ind [XIII]
Gleecol/-/29/5Ind [XIII]
Leathercol/-/29/5Ind [XIII]
1RHA/4/1Arm [XIII]
ZZ/-/4/1Arm [XIII]
1/171Lt/-/4/1Arm [XIII]
6RTR/4/1Arm [XIII]
1RTR/4/1Arm [XIII]
9L/4/1Arm [XIII]
1/KRRC/4/1Arm [XIII]
107RHA/22/1Arm [XIII]
287/102/22/1Arm [XIII]
50/Recce/22/1Arm [XIII]
B/QueensBay/22/1Arm [XIII]
C/QueensBay/22/1Arm [XIII]
3CLY/22/1Arm [XIII]
4CLY/22/1Arm [XIII]
2/RL/3(mot)Ind/- [XIII]
11/PAVOCR/3(mot)Ind/- [XIII]
18/KEOCR/3(mot)Ind/- [XIII]
4RHA/7(mot)/1Arm [XIII]
289/102ATk/7(mot)/1Arm [XIII]
2/RB/7(mot)/1Arm [XIII]
2/KRRC/7(mot)/1Arm [XIII]
9/KRRC/7(mot)/1Arm [XIII]
A/KDG/-/1Arm [XIII]
B/KDG/-/1Arm [XIII]
4FdRAA/4/2NZ [XIII]
5FdRAA/5/2NZ [XIII]
6FdRAA/Res/2NZ [XIII]
31/7ATk/4/2NZ [XIII]
32/7ATk/5/2NZ [XIII]
33/7ATk/Res/2NZ [XIII]
41Lt/-/4/2NZ [XIII]
42Lt/-/5/2NZ [XIII]
43Lt/-/Res/2NZ [XIII]
2MG/-/-/4/2NZ [XIII]
4MG/-/-/5/2NZ [XIII]
1MG/-/-/Res/2NZ [XIII]
28/-/4/2NZ [XIII]
19/-/4/2NZ [XIII]
20/-/4/2NZ [XIII]
21/-/5/2NZ [XIII]
22/-/5/2NZ [XIII]
23/-/5/2NZ [XIII]
18/-/Res/2NZ [XIII]
6Fd/-/-/4/-/5/2NZ [XIII]
7Fd/-/-/5/-/5/2NZ [XIII]
B/Cav/-/2NZ [XIII]
Rommel, Kampstaffel Kiehl
II/115/221/Arko104 [-]
408/221/Arko104 [-]
902/221/Arko104 [-]
580(mot)/-/-/90Le [DAK]
361/-/-/90Le [DAK]
288zbV/-/90Le [DAK]
155Schutz(mot)/-/90Le [DAK]
115Schutz(mot)/-/15Pz [DAK]
104Schutz(mot)/-/21Pz [DAK]
8/-/15Pz [DAK]
5/-/21Pz [DAK]
33(mot)/-/-/15Pz [DAK]
3(mot)/-/-/21Pz [DAK]
33/-/15Pz [DAK]
155/-/21Pz [DAK]
Trento [XXI] deployment zone
Sabr. [XXI] deployment zone
Pavia [X] Deployment zone
Brescia [X] Deployment zone
Littorio [XX] Deployment zone
Ariete [XX] Deployment zone
Trieste [XX] Deployment zone
Harass enemy 1st Arm Div
El Adem Airfield to the W
