Tobruk - Ras El Madauur
73SqnRAAF/202Gp WDAF
6Sqn/202Gp WDAF
45Sqn/202Gp WDAF
55Sqn/202Gp WDAF
III./StG1 X.
51FldRA/-/- [Cyrcom]
104RHA/SpGp/2Arm [Cyrcom]
107RHA/SpGp/2Arm [Cyrcom]
1RHA/SpGp/2Arm [Cyrcom]
B,O/1RHA/SpGp/2Arm [Cyrcom]
Red Line
Blue Line
Green Line
Bush Aty 20/9Aus [Cyrcom]
Bush Aty 24/9Aus [Cyrcom]
Bush Aty 26/9Aus [Cyrcom]
2/3/-/9Aus [Cyrcom]
20/-/-/20/9Aus [Cyrcom]
24/-/-/24/9Aus [Cyrcom]
26/-/-/26/9Aus [Cyrcom]
16/-/-/18/7Aus [Cyrcom]
14LAA/4AA/- [Cyrcom]
153/51Hvy/4AA/- [Cyrcom]
235/51Hvy/4AA/- [Cyrcom]
39/13LAA/4AA/- [Cyrcom]
40/13LAA/4AA/- [Cyrcom]
57/13LAA/4AA/- [Cyrcom]
2/13/20/9Aus [Cyrcom]
A/2/15/20/9Aus [Cyrcom]
A/2/17/20/9Aus [Cyrcom]
2/28/24/9Aus [Cyrcom]
2/23/24/9Aus [Cyrcom]
2/43/24/9Aus [Cyrcom]
2/24/26/9Aus [Cyrcom]
2/48/26/9Aus [Cyrcom]
2/9/18/7Aus [Cyrcom]
2/10/18/7Aus [Cyrcom]
2/12/18/7Aus [Cyrcom]
2/3Fld/-/-/9Aus [Cyrcom]
2/4Fld/-/-/7Aus [Cyrcom]
2/7Fld/-/-/9Aus [Cyrcom]
2/13Fld/-/-/9Aus [Cyrcom]
1Libyan/-/- [Cyrcom]
2Libyan/-/- [Cyrcom]
4Libyan/-/- [Cyrcom]
1KDG/3/2Arm [Cyrcom]
3H,5RTR/3/2Arm [Cyrcom]
B/1RTR/3/2Arm [Cyrcom]
C/1RTR/3/2Arm [Cyrcom]
4RTR/3/2Arm [Cyrcom]
1NF/-/-/- [Cyrcom]
M/3RHA/-/- [Cyrcom]
J/3RHA/-/- [Cyrcom]
18Cav/3Ind(Mot)/- [Cyrcom]
Fort Perrone
Fort Airenti
Fort Solaro
Fort Marcuca
Fort Pilastrino
Goschen's House
Wadi Auda Pumping Station
Distilleries
Sidi Mahmud
Tobruk
2/17/20/9Aus [Cyrcom]
C/2/17/20/9Aus [Cyrcom]
D/2/17/20/9Aus [Cyrcom]
2/15/20/9Aus [Cyrcom]
C/2/15/20/9Aus [Cyrcom]
D/2/15/20/9Aus [Cyrcom]
Red Line
1/62/Sicilia/102Trento [XXI]
2/62/Sicilia/102Trento [XXI]
3/62/Sicilia/102Trento [XXI]
4Uragano/XXXI/-/-/27Brescia
3Folgore/XXXI/-/-/132Ariete
1/19/Brescia/27Brescia [XXI]
2/19/Brescia/27Brescia [XXI]
3/19/Brescia/27Brescia [XXI]
1/20/Brescia/27Brescia [XXI]
2/20/Brescia/27Brescia [XXI]
3/20/Brescia/27Brescia [XXI]
1/55/-/27Brescia [XXI]
2/55/-/27Brescia [XXI]
3/55/-/27Brescia [XXI]
401/-/-/-/27Brescia [XXI]
404/-/-/-/27Brescia [XXI]
MG.Btl.2/200/5Le [DAK]
I./75/5Le [DAK]
I/115/15/15Pz [DAK]
5,6.(Pi.)/MG.Btl.2/200/5Le
I./5Pz/5Le [DAK]
II./5Pz/5Le [DAK]
1./II./5Pz/5Le [DAK]
2./I./5Pz/5Le [DAK]
King's Cross
To Bardia
To El Adem
To Derna
Ras El Madauur
Carrier Hill
El Gubbi Airfield
Wadi Sehel
Wadi Auda
III./StG1 X.
II./StG2 X.
3/20/Brescia/27Brescia[XXI]
1,2.(Pi.)/33/-/15Pz [DAK]
4RTR/3/2Arm [Cyrcom]
D/7RTR/3/2Arm
Harbour
Mare Nostrum
