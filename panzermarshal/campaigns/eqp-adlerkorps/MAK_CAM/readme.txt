Mussolini's Afrika Korps (MAK) campaign.

This campaign took five years to make - including a one year break to work on von Schlieffen. Five years during which I got married and we got 2 kids. This and even more reading just about explains why it took 2.5 times longer to finish the campaign than it took to make Das Reich - even though Luis' Suite makes campaign making easier these days compared to Lasse's tools that I was using making most of Das Reich.

The campaign is the fourth I made. Langemarck was a try out of what is possible with this game, Das Reich is my Blitzkrieg type campaign, von Schlieffen - of which I start making an updated version soon - is a WW1 campaign, MAK is an Italian desert campaign that starts with the Mussolini declaring war on England in September 1940 and ends with defeat in Tunisia or victory in Palestine mid 1943. I got inspired to make this campaign by the book "Mussolini's Afrika Korps - The Italian Army in North Africa 1940-1943" by Rex Trye and bought many other books reading my way through the desert war. As a player you are only allowed to buy Italian troops, but you'll find yourself accompanied by the best of the DAK the moment they set foot on African soil.

I found the Italian equipment in Adler's efile really interesting. Different testers of the campaign ended with very different core compositions. I ended my latest tests with this core:
Infantry
 MVSN , 5 bars , 1042 exp , 10 base , 8 curr , Infiltration Tactics - Ignores entr , 17 kill , 36 pp 
 MVSN , 5 bars , 678 exp , 10 base , 8 curr , Ferocious Defender - No unit can ig , 15 kill , 36 pp 
 Fanteria Motorizzata[F626] , 5 bars , 978 exp , 10 base , 10 curr , Determined Defence - +2 GD, +2 AD , 25 kill , 144 pp 
*  Genio Guastatori[F626] , 5 bars , 1034 exp , 10 base , 5 curr , Determined Defence - +2 GD, +2 AD , 20 kill , 0 pp 

Tank
 M15/42 , 5 bars , 1535 exp , 10 base , 9 curr , Fire Discipline - Consumes 1/2 ammo , 124 kill , 384 pp 
 M15/42 , 5 bars , 729 exp , 10 base , 11 curr , Overwatch - First unit that gets wi , 37 kill , 384 pp 

Recon
 AB-41 , 5 bars , 523 exp , 10 base , 5 curr , Skilled Reconnaissance - +1 spottin , 17 kill , 372 pp 

Anti-tank
*  Sem 47/32 L40 , 5 bars , 503 exp , 10 base , 9 curr , Combat Support - Lends half of its  , 16 kill , 0 pp 
*  Sem 75/18 M42 , 4 bars , 420 exp , 10 base , 6 curr , Street Fighter - Ignores entrenchme , 5 kill , 0 pp 

FlaK
 90mm 90/53 Camion , 5 bars , 965 exp , 10 base , 13 curr , Combat Support - Lends half of its  , 38 kill , 348 pp 
 20mm 20/65 Camion , 4 bars , 405 exp , 10 base , 9 curr , 5 kill , 192 pp 
 20mm 20/65 Camion , 2 bars , 275 exp , 10 base , 8 curr , 2 kill , 192 pp 

Artillery
 75mm 75/18[F626] , 5 bars , 508 exp , 10 base , 14 curr , 7 kill , 192 pp 
 75mm 75/18[F626] , 5 bars , 1300 exp , 10 base , 15 curr , Aggressive Maneuver - +1 movement , 20 kill , 192 pp 
 149mm 149/28[Tipo 61] , 5 bars , 1025 exp , 10 base , 15 curr , Infiltration Tactics - Ignores entr , 26 kill , 612 pp 

Fighter
*  MC.202 , 5 bars , 1134 exp , 10 base , 3 curr , Skilled Ground Attack - Bonus on at , 29 kill , 0 pp 

Tactical Bomber
*  Ju-87B-1 , 5 bars , 1552 exp , 10 base , 7 curr , Combat Support - Lends half of its  , 16 kill , 0 pp 


Make sure you check out the 75mm and 90mm Flak camions! They have their shortcomings but pack a bigger punch than any other type of equipment. They were the only thing that the Italians had that could stop 8th Army tanks.


I really need to thank the many people that contributed to this campaign:
- Testers: PzManiac, 1Terminator, Dragdasan and Patrick Steinmann.
- E-file maker: Adler. Also for adding the following units that I can remember to the file (most of these units still  exclusively used in this campaign):
	- BE 43 Force Publique (Egypt)
	- BE 37mm AT (Egypt)
	- BE 81mm mortar (Egypt)
	- BE Chevrolet Transport (Egypt)
	- FR P40F fighter (Tunisia)
	- DE 15sFH auf GWLrS(f) 
	- AU Kittyhawk IA
	- DE Bf 109F-4/B Jabo
	- DE Diana
	- NZ Lt VI
 	- All Countries LRDG
	- GB 2Pdr Portee
I have included version beta 0.7 of the efile in the campaign download, but any later version should work fine as well.
- Map makers: Without a doubt Dragdasan for making wonderful Tobruk and Karachoum maps for this campaign. 
- Smack files: Thanks to Dragdasan for making them and Adler for hosting them at http://adlerkorps.teathree.net/
- Music files: Thanks to Adler for hosting them at http://adlerkorps.teathree.net/
- OoB and story flow: Adler and Leon.

Sincere appologies to those that I might have not mentioned here like maybe testers of early versions of MAK (Dennis, Juankar?).


Install info:	
PG2 PATCH LEVEL: 1.02 G
SMACK (SMK) VIDEO FILES to be installed in the smack folder (recommended optional): MAK-Loss, MAK-End1, MAK-End2 and MAK-BV
MUSIC (MUS) FILES to be installed in the Sound folder(recommended optional): Folgore, Omalta, Dettomus, Sanmarco, Giovanif, M1, Afrinost, Tripoli, Rommel, Panzeria, Heiasafari, Itlili, Vincere2, Waltzing, Allarmi2, Veralyn, Dietrich
MAPs (SHP) FILES to be installed in the map folder. Download from this link that lists them all on screen http://pg2mapfinder.gilestiel.eu/index?214,49,131,615,380,490,300,117,591,214,320,641,652,591,653,346,2,369,43,369,620,47,688,462,203,626,38,594 


Please send feedback and questions to jurgensmet@yahoo.com.

Have fun with the game.
Jurgen (Doc).