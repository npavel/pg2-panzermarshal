C3 - Operation Theseus
Marfa
St Paul's Bay
Mellieha
Sliema
Musta
Birkirkara
Vallena
Hamrun
Zabbar
Citta Vecchia Rabat
Zebbuj
Luga
Paula Tarshim
Zeitjun
Sijuwi
Im Kabba
Safi
Zurrieq
South Comino Channel
Mellieha Bay
Marsaxlokk Harbor
Grand Harbor
Marsamxett Harbor
Mare Nostrum
Fort Bingemma-10/4H/CoastRA
Fort Madalena-10/4H/CoastRA
FortSanLeonardo-6/4H/CoastRA
Fort Benghisa-6/4H/CoastRA
Fort Delimara - 1/1H RMA
Fort San Rocco - 1/1H RMA
Fort Tigne - 2/1H RMA
Fort Campbell - 2/1H RMA
Fort St. Elmo - 3/1H RMA
Fort Ricasoli - 3/1H RMA
233Sqn Gibraltar
202SqnRAF Gibraltar
10SqnRAAF Gibraltar
801Sqn Eagle
813Sqn Eagle
800Sqn Indomitable
880Sqn Indomitable
806Sqn Indomitable
809Sqn Victorious
884Sqn Victorious
885Sqn Victorious
804Sqn Furious
38 Spits - Furious to Malta
126Sqn Malta
185Sqn Malta
229Sqn Malta
249Sqn Malta
402SqnRCAF Malta
252Sqn Malta
830Sqn Malta
38Sqn Malta
Safi airstrip
Luqa airfield
Ta Kali airfield
Hall Far airfield
4HAA Rgt RA
7HAA Rgt RA
10HAA Rgt RA
4/2HAA Rgt RMA
11/11HAA Rgt RMA
12/11HAA Rgt RMA
5/2HAA Rgt RMA
32LAA Rgt RA
65LAA Rgt RA
74LAA Rgt RA
6/3LAA Rgt RMA
7/3LAA Rgt RMA
225LAA Bty RA
15/40 26Defence Rgt
48/71RA 26Defence Rgt
13RMA 26Defence Rgt
2/Devon/Southern Command
1/Dorset/Southern Command
1/Hants/Southern Command
3/KOMR/Southern Command
8/Manch/Northern Command
2/RIrF/Northern Command
1/KOMR/Northern Command
2/KOMR/Northern Command
2/QORWK/Central Command
D/2/QORWK/Northern Command
11/LanFus/Central Command
10/KOMR/Central Command
1/Ches/Central Command
MP/-/-/Central Command
4/Buffs/Western Command
1/DLI/Western Command
8/KORR/Western Command
1Indep./44RTR
Cliffs
ConvoyEscortCruisers-ForceF
Convoy Escort DDs - Force F
Nelson and Rodney-ForceZ
CoveringForceCruisers-ForceZ
Victorious - Force Z
Furious - Force Z
Eagle - Force Z
Indomitable - Force Z
CoveringForceDDs - Force Z
Unbroken
Utmost
United
15 Cruiser Sqn Med.Fleet
14 DD Flot. Med.Fleet
12 DD Flot. Med.Fleet
Hebe, Speedy, Hythe and Rye 
II/1/-/Folgore [XI]
III/1/-/Folgore [XI]
IV/1/-/Folgore [XI]
V/2/-/Folgore [XI]
VI/2/-/Folgore [XI]
VII/2/-/Folgore [XI]
IX/3/-/Folgore [XI]
X/3/-/Folgore [XI]
VIII/-/-/Folgore [XI]
Co Mortai/-/-/-/Folgore [XI]
I/ArtParac/-/Folgore [XI]
II/ArtParac/-/Folgore [XI]
III/ArtParac/-/Folgore [XI]
Btg "P"/San Marco/-/- [XXX]
Bafile/San Marco/-/- [XXX]
Grado/San Marco/-/- [XXX]
XLIII/CNdaSbarco/- [XXX]
LX/CNdaSbarco/- [XXX]
XLII/CNdaSbarco/- [XXX]
L/CNdaSbarco/- [XXX]
V/CNdaSbarco/- [XXX]
La Spezia AirTransportToMalta
I/FJR.5/-/7Flieger [XI]
II/FJR.5/-/7Flieger [XI]
III/FJR.5/-/7Flieger [XI]
I/FJR.3/-/7Flieger [XI]
I/FJR.2/-/7Flieger [XI]
II/FJR.2/-/7Flieger [XI]
III/FJR.2/-/7Flieger [XI]
FsPioBtn/-/-/7Flieger [XI]
FsLehrBtlXI.FlK/-/-/7Flieger [XI]
II/FsArtRgt/-/7Flieger [XI]
III/200Pio
1./66Pz z.b.V.
2./66Pz z.b.V.
I/-/-/1Superga [XXX]
III/5/-/1Superga [XXX]
I/91/-/1Superga [XXX]
II/91/-/1Superga [XXX]
III/91/-/1Superga [XXX]
I/92/-/1Superga [XXX]
II/92/-/1Superga [XXX]
III/92/-/1Superga [XXX]
IV/5/-/1Superga [XXX]
II/5/-/1Superga [XXX]
IV/-/-/4Livorno [XXX]
III/28/-/4Livorno [XXX]
II/28/-/4Livorno [XXX]
IV/28/-/4Livorno [XXX]
XI/-/-/4Livorno [XXX]
33/-/4Livorno [XXX]
II/33/-/4Livorno [XXX]
III/33/-/4Livorno [XXX]
34/-/4Livorno [XXX]
II/34/-/4Livorno [XXX]
III/34/-/4Livorno [XXX]
LXXXVIII/88/-/20Friuli [XXX]
XCVI/88/-/20Friuli [XXX]
V/-/-/20Friuli [XXX]
87/-/20Friuli [XXX]
II/87/-/20Friuli [XXX]
III/87/-/20Friuli [XXX]
88/-/20Friuli [XXX]
II/88/-/20Friuli [XXX]
III/88/-/20Friuli [XXX]
II/35/-/20Friuli [XXX]
III/35/-/20Friuli [XXX]
IV/35/-/20Friuli [XXX]
DLV/-/10Arm/- [XXX]
LXX/10Arm/- [XXX]
IV/-/10Arm/- [XXX]
XXX/-/10Arm/- [XXX]
SubDiv46/4Sqn/SubFleet
20.a Squadriglia MAS 
7Sqn/2Fleet
3. Schnellbootflottille 
11/3rdSqn/2ndFleet
9/1Sqn/1stFleet
I/JG27 [II]
II/JG27 [II]
III/JG27 [II]
II/JG53 [II]
III/JG53 [II]
I/StG3 [II]
II/StG3 [II]
III/StG3 [II]
KG77 [II]
Kue.Fl.Gr.606 [II]
Kue.Fl.Gr.608 [II]
KG54 [II]
4.(H)/12 [II]
209, 239 Squadriglie
6Gruppo/1Stormo
131Gruppo/2Stormo
