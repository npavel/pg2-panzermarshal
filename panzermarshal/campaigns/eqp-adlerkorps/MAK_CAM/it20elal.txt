First El Alamein
Sidi Abd el Rahman
Kidney Ridge
Miteiriya Ridge
El Alamein
El Taqa Plateau
Alem el Onsol
Ruweisat Ridge
Pt. 64
Deir el Shein
Alam el Halfa Ridge
Qaret el Abd (Kaponga)
Qarrat el Himeimat
Tel el Eisa
Tel el Mukh Khad
Springbok Track
Alam Nayid
Deir el Munassib
El Taqa Plateau
Qattara Track
Coastal Road
Deir el Abyad
Ragil Depression
Samaket Gabata
Mare Nostrum
Kaponga Box
12SqnSAAF/3WingSAAF [WDAF]
24SqnSAAF/3WingSAAF [WDAF]
223SqnSAAF/3WingSAAF [WDAF]
1FldCFA SAA/-/1SA [XXX]
4FldSAA/-/1SA [XXX]
7FldSAA/-/1SA [XXX]
1LtSAA/-/1SA [XXX]
1SAA/-/1SA [XXX]
1FldSAE/-/-/-/1SA [XXX]
2FldSAE/-/-/-/1SA [XXX]
3FldSAE/-/-/-/1SA [XXX]
5FldSAE/-/-/-/1SA [XXX]
President Steyn/-/1SA [XXX]
3SARecon/-/-/1SA [XXX]
1/DukeEdinburghOR/1/1SA [XXX]
1/RoyalNatalCarab/1/1SA [XXX]
1/TransvaalScottish/1/1SA [XXX]
1/CapeTownHighl/2/1SA [XXX]
1/NatalMntdRifles/2/1SA [XXX]
1/2 Fld Force/2/1SA [XXX]
2Botha/-/1SA [XXX]
1/ImpLtHorse/3/1SA [XXX]
1/RandLI/3/1SA [XXX]
1/RoyalDurbanLI/3/1SA [XXX]
HQ 1SA [XXX]
2/5Essex/18Ind(att)/1SA [XXX]
2/3Gurkha/18Ind(att)/1SA [XXX]
4/11Sikh/18Ind(att)/1SA [XXX]
ATk/-/-/18Ind(att)/1SA [XXX]
Fld/18Ind(att)/1SA [XXX]
HQ 18Ind(att)/1SA [XXX]
RobCol-Remnants 10Ind [XXX]
22/1Arm [XXX]
4/1Arm [XXX]
1RD/-/1Arm [XXX]
Naqb el Khadim
2/WYork/9/5Ind [XIII]
3/9JAT/9/5Ind [XIII]
3/14Punjab/9/5Ind [XIII]
4/6Rajputana/5/5Ind [XIII]
3/10Baluch/5/5Ind [XIII]
1/4Essex/5/5Ind [XIII]
1/1Punjab/161(mot)/5Ind [XIII]
1/2Punjab/161(mot)/5Ind [XIII]
3/7Rajput/161(mot)/5Ind [XIII]
6FldRAA/6/2NZ [XIII]
43Lt/-/6/2NZ [XIII]
33/7ATk/6/2NZ [XIII]
1MG/-/-/6/2NZ [XIII]
24/-/6/2NZ [XIII]
25/-/6/2NZ [XIII]
26/-/6/2NZ [XIII]
4FldRAA/4/2NZ [XIII]
5FldRAA/5/2NZ [XIII]
2MG/-/-/4/2NZ [XIII]
4MG/-/-/5/2NZ [XIII]
41Lt/-/4/2NZ [XIII]
42Lt/-/5/2NZ [XIII]
31/7ATk/4/2NZ [XIII]
32/7ATk/5/2NZ [XIII]
19/-/4/2NZ [XIII]
20/-/4/2NZ [XIII]
28/-/4/2NZ [XIII]
21/-/5/2NZ [XIII]
22/-/5/2NZ [XIII]
23/-/5/2NZ [XIII]
HQ 2NZ [XIII]
Sabratha [XXI] depl.
Trento [XXI] depl.
Brescia [X] depl.
Pavia [X] depl.
Ariete [XX] depl.
Trieste [X] depl.
Littorio [X] depl.
Cav/-/2NZ [XIII]
2/28 /24/9Aus [XXX]
2/32 /24/9Aus [XXX]
2/43 /24/9Aus [XXX]
Sqn/44RTR/-/- [XXX]
2/23 /26/9Aus [XXX]
2/24 /26/9Aus [XXX]
2/48 /26/9Aus [XXX]
6RTR/-/- [XXX]
40RTR/23A/- [XXX]
46RTR/23A/- [XXX]
Rommel, Kampstaffel Kiehl
II/115/221/Arko104 [-]
408/221/Arko104 [-]
902/221/Arko104 [-]
580(mot)/-/-/90Le [DAK]
361/-/-/90Le [DAK]
288zbV/-/90Le [DAK]
155Schutz(mot)/-/90Le [DAK]
115Schutz(mot)/-/15Pz [DAK]
104Schutz(mot)/-/21Pz [DAK]
8/-/15Pz [DAK]
5/-/21Pz [DAK]
33(mot)/-/-/15Pz [DAK]
3(mot)/-/-/21Pz [DAK]
33/-/15Pz [DAK]
155/-/21Pz [DAK]
606(att)/-/-/90Le [DAK]
382/-/164Le Afrika [DAK]
StG 3 Afrika
JG 27 Afrika
450SqnRAAF/239Wing/211Gp
