Bzura Counterattack
Lodz
Zgierz
Lesmierz
Leczyca
Marynki
Tum
Shigi
Bryski
Striga
Biala
Piatek
Bzura River
26 R/30 ID/-/[8 Armee]
46 R/30 ID/-/[8 Armee]
30 AR/30 ID/-/[8 Armee]
30 AR/30 ID/-/[8 Armee]
30 Pion/30 ID/-/[8 Armee]
30 Rec/30 ID/-/[8 Armee]
68 R/17 ID/[Poznan]
69 R/17 ID/[Poznan]
70 R/17 ID/[Poznan]
17 Hvy/17 ID/[Poznan]
17 Lt/17 ID/[Poznan]
17 AA/17 ID/[Poznan]
72 Tk.C./17 ID/[Poznan]
17 E/17 ID/[Poznan]
55 R/14 ID/[Poznan]
57 R/14 ID/[Poznan]
58 R/14 ID/[Poznan]
14 E/14 ID/[Poznan]
14 Hvy/14 ID/[Poznan]
15 Lt/14 ID/[Poznan]
14 E/14 ID/[Poznan]
71 Tk.C./14 ID/[Poznan]
15L/W. Bde/[Poznan]
56 R/25 ID/[Poznan]
25 Hvy/25 ID/[Poznan]
25 Lt/25 ID/[Poznan]
[Poznan]
[Poznan]
Polish Supply Point
60 R/25 ID/[Poznan]
29 R/25 ID/[Poznan]
25 E/25 ID/[Poznan]
Krotoszyn/25 ID/[Poznan]
Ostrow/25 ID/[Poznan]
Oborniki/14 ID/[Poznan]
Opalenica/14 ID/[Poznan]
Poznan I/14 ID/[Poznan]
Poznan II/14 ID/[Poznan]
Szamotuly/14 ID/[Poznan]
Polish Bridgehead
Farm
7/W. Bde/[Poznan]
17L/W. Bde/[Poznan]
7R/W. Bde/[Poznan]
10/W. Bde/[Poznan]
71/W. Bde/[Poznan]
23 Pz R/17 ID/[XIII]
21 R/ 17 ID/[XIII]
55 R/ 17 ID/[XIII]
95 R/ 17 ID/[XIII]
17 R/ 17 ID/[XIII]
17 PzAbw/ 17 ID/[XIII]
17 Pion/ 17 ID/[XIII]
561 PzJgr/-/-/[8 Armee]
6 R/30 ID/-/[8 Armee]
31 R/24 ID/[XIII]
32 R/24 ID/[XIII]
102 R/24 ID/[XIII]
24 Pion/24 ID/[XIII]
24 PzAbw/24 ID/[XIII]
24 AR/24 ID/[XIII]
213 Pion/213 ID/-/[SUD]
318 R/213 ID/-/[SUD]
354 R/213 ID/-/[SUD]
406 R/213 ID/-/[SUD]
213 AR/213 ID/-/[SUD]
213 PzAbw/213 ID/-/[SUD]
213 Aufkl/213 ID/-/[SUD]
30 Grenz. R/-/[8 Armee]
