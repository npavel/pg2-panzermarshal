New Guinea: Northern coast, Buna-Gona
Dec 1942

You will be on your own for the up coming battle.  All spare troops are being rushed to Guadalcanal to assist in throwing out the US Marines.  Once Guadalcanal is secured we will shift the focus back onto New Guinea, your position will be the spring board for our future offensives.   


