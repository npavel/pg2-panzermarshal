Battle of Rangoon
7th Queen's Own
1st West Yorkshire
Gloucestershire
2/1st Heavy AA
1st Cameronians
1st West Yorkshire
5/17th Dogra
Force Viper
Royal Inniskilling
5th Indian AT
2nd Indian AT
1st Mountain Batt
2nd Mountain Batt
5th Mountain Batt
12th Indian Field
28th Mountain Batt
Rangoon Field
1/4th Indian Light 
2/4th Indian Light
3/4th Indian Light
2/13th FF 
8th Burma Rifle
FF1
FF2
FF3
FF6
Bhoma
Chin Hills
Kokine
Myitkyina
Nth Shan
Reserve FF
1/9th Jat
24th Royal Bombay
60th Madras Sappers
70th Bengal Sappers
5/3rd Gurka
1/3rd Queen Alex
2/5th Royal Gurka
1/4th Prince of Wale
Duke of Wellington
2/4 FF
3/4 FF
4/4 FF
8th Burma Rifles
1st Rangoon Police
2/13th FF Rifle
1/11th Sikh Reg
2/1st AT
2/1st Machine Gun
6th Div Cav
2/5th AIF
2/6th AIF
2/7th AIF
