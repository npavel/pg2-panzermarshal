Breakout though Mandalay: Northern Burma
May 1942

The following battle will be fought over some of the worst terrain you have encountered to date.  We expect that the terrain will slow up your advance more than the enemies troops.   This is the British last chance to stop us before the frontier of India.  

In return for some minor territories concession in Indochina the Thais have supplied a brigade of their elite royal troops to secure your right flank.   
