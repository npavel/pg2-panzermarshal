Failed diplomacy at Golan
March 1943

Your advances across the middle East completely unhinged the Allied positions in North Africa.  Rather than face both the Germans and us in Egypt the Allies have withdrawn from the whole theatre.

Now our Axis partners are demanding that we withdraw completely from the Middle East back to the Indian sub continent with strong hints about our fate should we not immediately comply.  These arrogant Germans need a lesson on the superiority of the Imperial Japanese Army.  



