Your actions bring everlasting glory to Japan.  Your victory inflicted such high losses on the Allies they no longer have the stomach for a second landing in Japan. 

The Emperor has announced that negotiations for an honourable end of the war with the Allies have commenced.  
