13. IWO JIMA, 02/19/1945

With Tinian in our grasp we are able to start B-29 bombing raids on the Japanese mainland. However we need a forward air base in which to place fighter escort squadrens and maitanence facilities for damaged aircraft. That's where Iwo Jima comes in. This will be the first time we have attacked the Japanese on what is considered traditional Japanese territory. The Japs know we're coming and have have ample time to dig in. We've asked the Navy for eight days of shelling they've been able to give us four.

You will join the 4th Marine division in assaulting Iwo Jima on BLUE Beach on the right flank of the 3rd Marines who are landing in the center on YELLOW Beach and the 5th Marines will be landing on the left flank taking Mt Suribachi. Your first objective is the Quarry and the coastal artillery that has been placed there.  Then swing north east and take Higashi linking with the 3rd Marines at Motoyama Airfield.

Secure a beachhead and air support, artilery and armor are right behind you. Good luck.

(note: this scenario made by Chris "PFC Lobo" Nies for this campaign)



