02. GUADALCANAL 3, 09/24/1942

Larger and larger concentrations of Japanese troops are appearing all around the perimeter; you are ordered to mount an attack to the west and engage Japanese units around the village of Matanikau and south along the Matanikau River.

NOTE: 
A loss in this scenario requires you to play it again.