05. NEW GEORGIA 1, 06/30/1943

The last Japanese units withdrew from Guadalcanal in February, however you are already training for the next operation. After the successful defense of Guadalcanal we will go on the offensive and your men will play important parts in our plans. 

Operation Toenails, the invasion of the island of New Georgia, is planned for July. Starting in late June three landings will be made on the eastern part of the island and later, at the start of July, the western part of the island will be occupied. You will participate in the western landings. The objective of these landings is the capture of the airfield on Rendova island and, most importantly, the airfield at Munda Point. A small force will capture Rendova while the rest of the operation will be split into two parts, the Northern and Southern Groups, on the main island. These will be largely Army operations, but your men will be used in important capacities in the western landings.

NOTE: 
You will not be able to deploy all your units in this scenario.


NOTE: 
A loss in this scenario requires you to play it again.