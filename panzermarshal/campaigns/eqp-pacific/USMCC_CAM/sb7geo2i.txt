06. NEW GEORGIA 2, 07/17/1943

The assaults on Munda airfield and Bairoko have stalled. Two major naval battles recently were fought to the NW of New Georgia, the Battles of Kula Gulf and Kolombangara, and have successfully deterred Japanese reinforcements. Once Army reinforcements arrive, your orders are to capture Munda airfield, regain Bairoko and clear Arundel Island of enemy troops. The island of Kolombangara (at the NW of the map) will be bypassed. Again, your troops will be used to support Army operations.

You have just received news that Japanese units have launched an assault on the southern beachhead...

NOTE: 
You will not be able to deploy all your units in this scenario.

NOTE: 
A loss in this scenario requires you to play it again.