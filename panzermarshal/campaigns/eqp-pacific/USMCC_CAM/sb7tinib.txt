BRILLIANT VICTORY

You had a much easier task than your colleagues on Saipan, yet your victory was not as easy as we expected. After continuous fighting for 2 years you will be pulled off the line for extensive rest and re-equipping. The war will not end any time soon, so no doubt we will need you in the future.