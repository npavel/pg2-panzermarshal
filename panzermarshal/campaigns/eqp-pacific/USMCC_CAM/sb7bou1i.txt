07. BOUGAINVILLE 1, 10/31/1943

The last objective in the Solomons is Bougainville, which has large Japanese concentrations at the north and south, including airfields, around Buin and Buka. In addition to being a major base any airfield on Bougainville is within easy striking distance of Rabaul. The plan is not to attack either of the Japanese bases, rather to land on the island in the center and build an airfield.

Your orders are to land on Cape Torokina and secure a beachhead. You should expect the Japanese to react strongly to this attack, although we hope to catch their ground troops off guard. Japanese naval forces are known to be in the area.