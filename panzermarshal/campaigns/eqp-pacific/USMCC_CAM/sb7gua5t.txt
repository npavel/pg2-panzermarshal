TACTICAL VICTORY

Your men have performed as expected, however you will now be withdrawn; other units will finish the battle. You will be resupplied and reequipped in preparation for the next battle.