VICTORY

Your men have performed beyond expectations, however you will now be withdrawn; other units will finish the battle. You will be resupplied and reequipped in preparation for the next battle.