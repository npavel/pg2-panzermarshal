VICTORY

Your men performed excellently. Your orders are to pull your men off the line to prepare for future offensives; the mopping up of New Georgia will be performed by new Army units.