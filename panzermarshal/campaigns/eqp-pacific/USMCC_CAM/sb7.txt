US MARINE CORPS CAMPAIGN, v2.00
US MARINE CORPS CAMPAIGN, v2.00

Version: 2.0
Equipment: Pacific Equipment File (v4.0+)
Number of Scenarios/Longest Path: 14/14
Player Country: US Navy/Marine Corps
Campaign Designer: Steve Brown
Extra Scenario: "Iwo Jima" by Chris Nies
Special Award: Mick Marchand ("Tinian" map)
Playtesters (for all versions): Peter Stone, Jesper Damgaard, Dennis Felling, Jan Hedstrom, Mick Marchand, Chris Nies, John White.

Maps needed:

Guadalcanal2       :   Talasea
New Georgia         :   Tinian
Cape Gloucester  :   Torokina
Iwo 

Please note that you want "Guadalcanal2," not "Guadalcanal" - and "Iwo," not "Iwojima" or "Iwo7."

The Campaign:

This campaign follows battles of the US Marine Corps from 1942-45. The campaign starts at Guadalcanal then follows the Solomons campaign (New Georgia and Bougainville), afterwards you will fight on New Britain (Cape Gloucester and Talasea) and finally move to the Central Pacific (Tinian) and finish on Iwo Jima.

Revision History:

1. Version 1.0, 03/2004
2. Version 1.0a, 04/2004
3. Version 2.0, 08/2005

Contact Information:

You can contact me and get the latest version of this campaign and the equipment file from:
steve@pg-2.com
www.pg-2.com