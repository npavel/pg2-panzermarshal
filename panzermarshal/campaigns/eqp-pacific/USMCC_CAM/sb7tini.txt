Tinian
Pacific Ocean
Ushi Point
White Beach Number 1
Ushi Point Airfield
White Beach Number 2
Mount Maga
Maga Town
Asiga Point
Faibus San Hilo Point
Mount Lasso
Yellow Beach
Mount Masalog
Masalog Point
Gurguan Point
Sunharon Harbor
Orange Beach
Red Beach/Tinian Town
Tinian Town
Tinian Airfield
Green Beach
Hill 580: MOUNTAIN
Hill 580: ESCARPMENT
Blue Beach
Hill 560: MOUNTAIN
Hill 580: FORTIFICATION
Marpo Point
Patterson Isle
Lalo Point
6 inch battery/56th Keibitai
523rd Air Group Constr. Bn
233rd Construction Bn
116th Construction Bn
833rd Construction Bn
AT/50
1,2/50
1,2/50
1,2/50
Tank Co/18th Infantry
Tank Co/18th Infantry
1,2/50
1,2/50
1,2/50
1st Artillery Battery/50
2nd Artillery Battery/50
Art/50
3/50
Art/50
82nd AD Group/56th Keibitai
82nd AD Group/56th Keibitai
Fortified Caves
HQ 23rd Air Group
HQ 50th Inf Regiment
19th Squadron USAAF 
83rd AD Group/56th Keibitai
1/135
1/135
1/135
50th Inf Regt
50th Inf Regt
50th Inf Regt
50th Inf Regt
50th Inf Regt
50th Inf Regt
56th Keibitai
56th Keibitai
56th Keibitai
56th Keibitai
2nd Armored Amphibian Bn
VB-15 (USS Essex)
44th Squadron USAAF
USS John White
USS Damgaard
708 Amphibian Tank Bn (Army)
Fortification Detachment/50
D/4th Tank Bn
Naval Air Stragglers
VF-15 (USS Essex)
HMAS Peter Stone
