TACTICAL VICTORY

Despite some hard fighting you have acheived your initial objectives. The villages of Matanikau and Kokumbona have been cleared and a nasty attack across the Tenaru has been stopped. The outlying villages will not be held permanently, so pull your men back to the defense perimeter.