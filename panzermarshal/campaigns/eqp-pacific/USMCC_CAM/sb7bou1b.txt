BRILLIANT VICTORY

Despite some fighting the beachhead has been established. Your next campaign, the landing on New Britain, already is being planned - however you are to remain on Bougainville and hold the perimeter until replaced by the Army.