There are 2 readme files for this campaign (same information, different formats)

USSWPC100_readme.pdf (PDF version)
USSWPC100_readme.txt (text version)
-------------------------------------------------------------------------------
What is PDF? 

You may notice I have the readme available as a PDF file, this is Adobe's "Portable Document Format" which can be read with the free Adobe Acrobat Reader. This Reader is on most of the PG2 CDs (all versions except US 1.00 have the Acrobat Reader).

You can also download the Acrobat Reader from http://www.adobe.com/prodindex/acrobat/readstep.html.

The PDF version of the readme is much easier to read and contains direct links to web sites from the document itself.
-------------------------------------------------------------------------------
README Additions.

None so far.


Steve Brown
05/2008
