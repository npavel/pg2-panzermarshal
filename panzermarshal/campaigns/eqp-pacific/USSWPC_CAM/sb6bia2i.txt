BIAK 2 - June 19, 1944

Strategic Situation:
To your east, the Sarmi landings are progressing slowly and to your west a landing is planned on the nearby small island of Noemfoor to capture the airfield - specifically due to the fact that the Biak operation has bogged down. Further to the east, Japanese forces are still squeezed between US troops moving east from Aitape and Australians moving west from Madang.

Tactical Briefing:
Over the period between the initial invasion and today the Japanese Navy attempted to resupply Biak, with little success - clearly Biak is important to them. General MacArthur was displeased with the Biak operation and ordered the Noemfoor operation, at the cost of some lives, just to gain an airfield. The Biak airfields, while under your nominal control, are still under threat from the remnants of the Japanese garrison - who have retreated to caves in the hills. Your orders are to eliminate the Japanese threat once and for all, and gain full control of the airfields. Your men are needed for further operations and cannot be spared just to mop up. Rooting the Japs out of their caves will no doubt be hard, but you have your orders.