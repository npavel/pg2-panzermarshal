VICTORY

Despite hard fighting over Zig Zag Pass, you have managed to isolate Bataan. You should prepare for more fighting while the rest of your men catch up with you.