MOROTAI 2 - December 24, 1944

Strategic Situation:
The invasion of the Philippines continues.

Tactical Briefing:
The airfields on Morotai have become vital to the ongoing operations in the Philippines and must be defended. The Japanese strength on Morotai has been greatly reduced but they have been reinforced from Halmahera and could be a threat.

Your orders are to take the battle to the Japanese and wipe them out.

Most of your men are preparing for the invasion of Luzon so you will have a minimal force available to you.

