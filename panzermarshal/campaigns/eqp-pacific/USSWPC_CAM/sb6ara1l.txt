LOSS

Despite your loss, we at least have a small beachhead. You will have to do better in the future or you will threaten the Cape Gloucester landings.