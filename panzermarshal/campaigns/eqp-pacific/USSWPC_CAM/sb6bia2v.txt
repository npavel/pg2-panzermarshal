VICTORY

After a vicious battle you now are in full control of the airfields. There is still a lot of mopping up to do, but for now your men are pulled off the line in preparation for future offensives.