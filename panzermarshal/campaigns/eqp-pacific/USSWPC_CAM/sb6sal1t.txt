TACTICAL VICTORY

You have successfully linked with the Australian Army. Your orders are to delay the assault on Salamaua until the attack on Lae can begin.
