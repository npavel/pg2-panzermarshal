ZAMBOANGA - March 10, 1945

Strategic Situation:
Manila, and in fact Luzon, are more or less under control (although there is still a lot of fighting). The Japanese still control a number of bases in the southern Philippines that potentially threaten future operations.

Tactical Briefing:
In the far south of the Philippines a large Japanese detachment is based on the island of Mindanao. Your objective is the Japanese base at Zamboanga, which is at the southwest tip of Mindanao. Control of the Zamboanga area is considered to be of importance to future operations and its location is important for control of the sea lanes. Your orders are the are capture of Zamboanga and surrounding airfields plus control of high ground to the north.

YOU HAVE RECEIVED ORDERS:
You should establish a beachhead around the town of San Mateo and push inland from there. YOU WILL NOT BE ABLE TO LAND ANYWHERE ELSE!