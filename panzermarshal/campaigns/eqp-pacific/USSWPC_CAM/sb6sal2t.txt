TACTICAL VICTORY

Your victory is notable, although it is overshadowed by the offensive further north at Lae. Your green troops are now becoming veterans but are badly in need of rest, so you are again pulled off the line to Australia for retraining. Part of your training will be for amphibious landings and you have more equipment available.