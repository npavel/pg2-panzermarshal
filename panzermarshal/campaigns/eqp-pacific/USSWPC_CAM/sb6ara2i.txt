ARAWE 2 - January 1, 1944

Strategic Situation:
Both Operation Dexterity and Operation Director have gone as planned, although the Marines at Cape Gloucester are still fighting and are taking the brunt of the Japanese attacks. On New Guinea, the Australians have captured Finschhafen and are advancing towards their next objective; Madang.

Tactical Briefing:
You will remain on the Arawe Peninsula. After the initial beachhead was established most of your Naval support withdrew to help with the Marine landings, you must now hold the peninsula without their assistance. Despite the fact that a lot of Japanese are concentrating on Cape Gloucester a significant force has dug in at the edge of the peninsula and must be dealt with. Your orders are to break this force; a secondary objective is to expand the beachhead east and west. Once your operations are complete your men will be replaced.

You will receive reinforcements from the 158th Infantry and some Marine tanks; air support will be provided if it can be spared. The Japanese are not expected to reduce their air activity - so prepare your air defences.