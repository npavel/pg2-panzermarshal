TACTICAL VICTORY

Despite a lack of heavy weapons your men capture some of the outlying defences of Buna, although you have taken losses. The advance has been difficult and movement of supplies have been almost impossible. Your orders are to continue the assault on Buna.
