HOLLANDIA - April 22, 1944

Strategic Situation:
A decision has been made to stage two amphibious operations, at Aitape and Hollandia - both operations will bypass a major Japanese base at Wewak, which will be isolated.

Tactical Briefing:
You will command Operation Reckless, the landing at Hollandia. Your forces will be landed at Tanamerah and Humboldt Bays and must push inland to capture the airfields around Lake Sentani - control of these airfields is necessary to support future operations. The operation has been preceeded by large air strikes, but the quality of the remaining defenders is unknown.