LOSS

You are ordered to stop your offensive. It now seems that the troops you have faced are only a small detachment. Hold your positions and await further orders.