AITAPE 3 - August 1, 1944

Tactical Briefing:
Despite successes west of the Driniumor your men are still besieged around Afua - and fresh Japanese troops are moving into the area. After weeks of weeks of constant fighting there is no time to rest; you have received orders to cross the river near the coast and attempt to envelop the Japanese from the north. Afua is still just barely holding, so the sooner the defenders can be relieved the better...
