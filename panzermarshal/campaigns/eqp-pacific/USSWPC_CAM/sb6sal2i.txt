SALAMAUA 2 - August 16, 1943

Strategic Situation:
The assault on Salamaua has served the purpose of hiding the real objective of the Allied advance, the Lae and Nadzab operations. The Japanese have been forced to send troops south to defend Salamaua and weaken their positions around Lae.

Tactical Briefing:
Now is the time to capture Salamaua - the Australians will continue their advance from Wau, with the objective of capturing Salamaua and cutting the road to Lae. You will advance up the coast and assist in the assault on Salamaua.