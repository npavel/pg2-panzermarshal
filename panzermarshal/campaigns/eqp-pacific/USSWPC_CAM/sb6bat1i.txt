BATAAN 1 - January 15, 1942

Strategic Situation:
Japanese forces have taken most of the Philippines and have made significant gains in Malaya, Borneo, Thailand and elesewhere. The chances of relief are getting worse all the time.

Tactical Briefing:
US forces on Luzon are now confined to the Bataan Peninsula. Your orders are to hold the main defensive line from Mauban (in the west) to Abucay (in the east). 