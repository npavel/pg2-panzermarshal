#!/bin/sh
if [ $# -eq 0 ]; then
    echo "Usage: $0 CAMPAIGN_FOLDER to check"
    exit 1
fi

today=$(date +%Y-%m-%d)
flist=$(ls -1a $1)
destname=$1_SYNC_$today
mkdir -p $destname
for i in $flist; do
    if [ $i == "." -o $i == ".." ]; then
	continue
    fi
    cp -v ../SCENARIO/$i $destname
done