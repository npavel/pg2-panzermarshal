#!/bin/sh

EQP_FILE="eqp-adlerkorps"
PM_ROOT="panzermarshal"

# This will prepare a sandbox folder for editing with SuitePG2 or IconConvert tools
echo "Select equipment file: "
echo "1) AdlerKorps"
echo "2) Pacific"

read eqp

case $eqp in
    1)
	echo "AdlerKorps Equipment selected"
	EQP_FILE="eqp-adlerkorps"
	break
    ;;
    2)
	echo "Pacific Equipment selected"
	EQP_FILE="eqp-pacific"
	break
    ;;
esac

TMPDIR="sandbox-$EQP_FILE"
mkdir -p $TMPDIR
mkdir -p $TMPDIR/SCENARIO
mkdir -p $TMPDIR/MAP

# Campaigns (.cam), scenarios (.scn) and maps (.map)
for i in $PM_ROOT/campaigns/$EQP_FILE/*_CAM; do
 for j in $i/*; do
    ln -sf "../../$j" $TMPDIR/SCENARIO/
 done
done

# Common .map files
for i in $PM_ROOT/maps/*; do
    ln -sf "../../$i" $TMPDIR/SCENARIO/
done

# PNG Maps
for i in $PM_ROOT/pngmaps/*; do
    ln -sf "../../$i" $TMPDIR/MAP/
done

# Equipment files
for i in $PM_ROOT/eqp/$EQP_FILE/*; do
    ln -sf ../$i $TMPDIR
done

# Graphics DAT file (only useful for editor)
for i in $PM_ROOT/dat/$EQP_FILE/*; do
    ln -sf ../$i $TMPDIR
done

# Editor
ln -sf ../$PM_ROOT/editors/shptool/PG2_ShpTool.exe $TMPDIR
ln -sf ../$PM_ROOT/editors/suitepg2/SuitePG2.exe $TMPDIR


